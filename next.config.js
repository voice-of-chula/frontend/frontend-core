const withOptimizedImages = require('next-optimized-images')

module.exports = withOptimizedImages({
  crossOrigin: 'anonymous',
  async redirects() {
    return [
      {
        source: '/proposal/:proposalId/:slug*',
        destination: '/:proposalId/:slug*',
        permanent: true,
      },
    ]
  },
})
