import React, {
  createContext,
  useContext,
  useState,
  useCallback,
  useEffect,
  useRef,
  PropsWithChildren,
} from 'react'
import { useRouter } from 'next/router'
import { logout as apiLogout } from '@/utils/api/auth'
import { httpClient } from '@/utils/api/http'
import { AxiosRequestConfig } from 'axios'
import { getLoginURL } from '@/utils'
import { PageSpinner } from '@/components/layout/PageSpinner'
import { TokenManager, AccessToken } from '@/utils/tokenManager'
import { resolveAuthPromise } from '@/utils/waitForAuth'

export interface AuthConstruct {
  accessToken: string
  ouid: string | null
  isAuthenticated: boolean
  isPending: boolean
  login: (token: string) => void
  logout: () => Promise<void>
  getAccessToken: () => Promise<string | null>
}

export const globalAuthState = {
  ouid: null as string | null,
}

export const AuthContext = createContext({} as AuthConstruct)

export function useAuthContext(): AuthConstruct {
  return useContext(AuthContext)
}

export function waitForAuth<P>(
  ComposedComponent: React.ComponentType<P>
): React.ComponentType<P> {
  return function waitForAuth(props: P) {
    const { isPending } = useAuthContext()

    if (isPending) {
      return <PageSpinner />
    }

    return <ComposedComponent {...props} />
  }
}

export function withAuth<P>(
  ComposedComponent: React.ComponentType<P>,
  ContentWrapper?: React.ComponentType<PropsWithChildren<{}>>
): React.ComponentType<P> {
  return function withAuth(props: P) {
    const router = useRouter()
    const loginURL = getLoginURL(router)
    const { isAuthenticated, isPending } = useAuthContext()

    useEffect(() => {
      if (!isAuthenticated && !isPending) {
        router.replace(loginURL)
      }
    }, [isAuthenticated, isPending, router, loginURL])

    let element: JSX.Element
    if (isAuthenticated) {
      element = <ComposedComponent {...props} />
    } else {
      element = <PageSpinner />
    }

    if (ContentWrapper) {
      return <ContentWrapper>{element}</ContentWrapper>
    } else {
      return element
    }
  }
}

const AuthProvider: React.FC<{
  tryRefreshToken: boolean
  children: React.ReactNode
}> = ({ tryRefreshToken, children }) => {
  const [accessToken, setAccessToken] = useState<AccessToken>(undefined)
  const tokenManager = useRef<TokenManager>(null)
  if (tokenManager.current === null) {
    const listener = (accessToken: AccessToken) => {
      setAccessToken(accessToken)
      if (typeof window !== 'undefined') {
        resolveAuthPromise()
      }
    }
    tokenManager.current = new TokenManager(tryRefreshToken, listener)
  }

  const logout = useCallback(async () => {
    await apiLogout()
    tokenManager.current.setToken(null)
  }, [])

  useEffect(() => {
    const tokenInjector = httpClient.interceptors.request.use(
      async (config: AxiosRequestConfig) => {
        const accessToken = await tokenManager.current.getAccessToken()
        if (accessToken) {
          config.headers.Authorization = `Bearer ${accessToken.accessToken}`
        }
        return config
      }
    )
    const signout = httpClient.interceptors.response.use(undefined, (error) => {
      if (error?.response?.status == 401) logout()
      return Promise.reject(error)
    })
    return () => {
      httpClient.interceptors.request.eject(tokenInjector)
      httpClient.interceptors.response.eject(signout)
    }
  }, [])

  const ouid: string | null = accessToken?.tokenData?.ouid || null

  const isAuthenticated =
    accessToken !== null && typeof accessToken !== 'undefined'

  const login = useCallback((newToken: string) => {
    tokenManager.current.setToken(newToken)
  }, [])

  const getAccessToken = useCallback(async () => {
    return (await tokenManager.current.getAccessToken()).accessToken
  }, [])

  const value: AuthConstruct = {
    accessToken: accessToken?.accessToken,
    ouid,
    isAuthenticated,
    isPending: typeof accessToken === 'undefined',
    login,
    logout,
    getAccessToken,
  }

  globalAuthState.ouid = ouid

  return <AuthContext.Provider value={value} children={children} />
}

export default AuthProvider
