import { createContext, PropsWithChildren, useContext } from 'react'
import { PageMeta } from '@/utils/pageMeta'
import { MenuItem } from '@/components/layout/NavBar'
import { useRouter } from 'next/router'
import { proposalPagePath } from '@/pages/[proposalId]'

export interface ProposalConstruct {
  proposalId: string
}

const ProposalContext = createContext(null as ProposalConstruct)

export function useProposalContext() {
  return useContext(ProposalContext)
}

export const ProposalProvider: React.FC<
  ProposalConstruct & { children: React.ReactNode }
> = ({ children, ...value }) => {
  const { proposalId } = value
  const base = proposalPagePath(proposalId)
  return (
    <ProposalContext.Provider value={value}>
      <PageMeta
        navBarItems={
          <>
            <MenuItem href={`${base}`}>หัวข้อรับฟังเสียง</MenuItem>
            <MenuItem href={`${base}/createVoice`}>ส่งเสียง</MenuItem>
            <MenuItem href={`${base}/myVoice`}>เสียงของฉัน</MenuItem>
          </>
        }
      />
      {children}
    </ProposalContext.Provider>
  )
}

export function RouterProposalProvider({ children }: PropsWithChildren<{}>) {
  const router = useRouter()
  const proposalId = router.query.proposalId as string
  return <ProposalProvider proposalId={proposalId}>{children}</ProposalProvider>
}
