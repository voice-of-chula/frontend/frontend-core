import { Box, Flex, Text } from '@chakra-ui/core'
import { PageContainer } from './PageContainer'
import { SafeArea } from '@/components/common/SafeArea'

const Footer: React.FC = () => {
  return (
    <SafeArea bg="chula.SecondaryPink" marginTop={4} overflowY="hidden">
      <PageContainer>
        <Flex paddingY={4} alignItems="center" justifyContent="space-between">
          <Flex direction="column">
            <Text
              fontFamily="ChulaCharasNew"
              fontWeight="bold"
              fontSize="40px"
              lineHeight="0.25em"
              transform="rotate(180deg) translateY(1.5rem)"
              textAlign="end"
              paddingBottom="0.25em"
              color="chula.Gray"
            >
              ”
            </Text>
            <Text
              fontFamily="ChulaCharasNew"
              fontWeight="bold"
              fontSize="14px"
              color="chula.Gray"
            >
              ด้วยความสำนึกในระบอบประชาธิปไตย
              <br />
              ที่ให้คุณค่าของทุกเสียงอย่างเท่าเทียมกัน
            </Text>
            <Text
              fontFamily="ChulaCharasNew"
              fontWeight="bold"
              fontSize="40px"
              lineHeight="0.25em"
              paddingTop="0.5em"
              color="chula.Gray"
            >
              ”
            </Text>
          </Flex>
          <Box>
            <Text
              fontFamily="ChulaCharasNew"
              fontWeight="bold"
              fontSize={['12px', '12px', '12px', '14px']}
              color="chula.Gray"
              lineHeight="16px"
              marginBottom="8px"
            >
              สงวนลิขสิทธิ์ © 2020
              <br />
              องค์การบริหารสโมสรนิสิตจุฬาลงกรณ์มหาวิทยาลัย
            </Text>
            <Text
              fontFamily="ChulaCharasNew"
              fontSize="12px"
              color="chula.Gray"
              lineHeight="16px"
            >
              ชั้น 2 อาคารจุลจักรพงษ์
              <br />
              ถนนพญาไท แขวงวังใหม่ เขตปทุมวัน
              <br />
              กรุงเทพมหานคร 10330
            </Text>
          </Box>
        </Flex>
      </PageContainer>
    </SafeArea>
  )
}

export { Footer }
