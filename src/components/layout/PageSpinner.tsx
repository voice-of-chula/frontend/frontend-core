import { CustomSpinner } from '@/components/common/CustomSpinner'
import { Flex } from '@chakra-ui/core'

const PageSpinner: React.FC = () => {
  return (
    <Flex flex={1} alignItems="center">
      <CustomSpinner />
    </Flex>
  )
}

export { PageSpinner }
