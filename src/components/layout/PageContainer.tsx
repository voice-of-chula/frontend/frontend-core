import { Flex, FlexProps } from '@chakra-ui/core'

const PageContainer: React.FC<
  {
    floating?: React.ReactNode
    children?: React.ReactNode
  } & FlexProps
> = ({ floating, children, ...props }) => {
  return (
    <Flex flexDirection="column" flex={1} paddingX={4}>
      <Flex
        as="main"
        alignSelf="center"
        flexDirection="column"
        flex={1}
        width={['full', 'full', '540px', '720px', '960px', '1140px']}
        {...props}
      >
        {children}
      </Flex>
      {floating}
    </Flex>
  )
}

export { PageContainer }
