import React from 'react'
import { Button, ButtonProps } from '@chakra-ui/core'

const NavBarButton = React.forwardRef<HTMLElement, ButtonProps>(
  (props, ref) => {
    return (
      <Button
        ref={ref}
        fontWeight="normal"
        variantColor="PrimaryPink"
        fontSize="16px"
        variant="outline"
        fontFamily="Sarabun"
        bg="chula.White"
        h="34px"
        w="116px"
        {...props}
      />
    )
  }
)

export { NavBarButton }
