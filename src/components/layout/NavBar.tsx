import {
  Box,
  BoxProps,
  Divider,
  Flex,
  Link,
  useDisclosure,
  useTheme,
} from '@chakra-ui/core'
import { transparentize } from '@chakra-ui/theme-tools'
import NextLink from 'next/link'
import { useRouter } from 'next/router'
import {
  createContext,
  forwardRef,
  useCallback,
  useContext,
  useEffect,
} from 'react'
import { useAuthContext } from '@/providers/AuthProvider'
import { getLoginURL } from '@/utils'
import { usePageMeta } from '@/utils/pageMeta'
import { PageContainer } from './PageContainer'
import { SafeArea } from '@/components/common/SafeArea'
import logo from '@/images/logo.svg'
import menu from '@/images/menu.svg'
import { NavBarButton } from './NavBarButton'
import { CustomSpinner } from '@/components/common/CustomSpinner'

interface ContextValue {
  onClose: () => void
}

const NavBarContext = createContext<ContextValue>(null)

const NavBarBox = forwardRef<
  HTMLElement,
  BoxProps & { children?: React.ReactNode }
>((props, ref) => (
  <Flex
    ref={ref}
    bg="chula.PrimaryPink"
    h={12}
    w="100%"
    flexDir="row"
    alignItems="center"
    {...props}
  />
))

const FixedNavBarBox = forwardRef<
  HTMLElement,
  BoxProps & { children?: React.ReactNode }
>((props, ref) => (
  <NavBarBox
    zIndex={100}
    ref={ref}
    position="fixed"
    top="0"
    left="0"
    {...props}
  />
))

type ToggleExpandProps = React.ImgHTMLAttributes<HTMLImageElement> & {
  showOnDesktop: boolean
}

const ToggleExpand: React.FC<ToggleExpandProps> = ({
  showOnDesktop,
  ...props
}) => {
  const breakpoints = [
    'initial',
    'initial',
    'initial',
    showOnDesktop ? 'initial' : 'none',
  ]

  return (
    <Box display={breakpoints} ml={3}>
      <img
        src={menu}
        alt="เมนู"
        style={{
          cursor: 'pointer',
          marginLeft: 4,
          marginRight: -5,
          maxWidth: 'none',
        }}
        {...props}
      />
    </Box>
  )
}

export interface MenuItemProps {
  children: React.ReactNode
  href?: string
  display?: Array<string>
  onClick?: () => void
}

export const MenuItem: React.FC<MenuItemProps> = ({
  children,
  href,
  display,
  onClick,
}: MenuItemProps) => {
  const { onClose } = useContext(NavBarContext)
  const router = useRouter()
  const active = href === router.pathname

  const link = (
    <Link
      display="block"
      width="100%"
      textAlign="center"
      fontFamily="CHULALONGKORN"
      fontSize="2xl"
      color="white"
      onClick={() => {
        if (onClick) {
          onClick()
        }
        active && onClose()
      }}
    >
      {children}
    </Link>
  )

  return (
    <Box display={display}>
      {href ? (
        <NextLink href={href} passHref>
          {link}
        </NextLink>
      ) : (
        link
      )}
      <Divider orientation="horizontal" />
    </Box>
  )
}

function NavBarMenuMobile() {
  const pageMeta = usePageMeta()
  const { isAuthenticated, logout } = useAuthContext()
  const router = useRouter()
  const loginURL = getLoginURL(router)
  const breakpoints = ['initial', 'initial', 'initial', 'none']

  return (
    <PageContainer overflowY="auto" maxHeight="calc(100vh - 48px)">
      <Divider orientation="horizontal" />
      <MenuItem href="/" display={breakpoints}>
        หน้าแรก
      </MenuItem>
      {pageMeta.navBarItems}
      <MenuItem href="/request" display={breakpoints}>
        ส่งคำขอเปิดประเด็น
      </MenuItem>
      <MenuItem href="/terms" display={breakpoints}>
        ข้อตกลงในการใช้งาน
      </MenuItem>
      {!isAuthenticated ? (
        <MenuItem href={loginURL} display={breakpoints}>
          เข้าสู่ระบบ
        </MenuItem>
      ) : (
        <MenuItem
          onClick={() => logout().then(() => window.location.reload())}
          display={breakpoints}
        >
          ออกจากระบบ
        </MenuItem>
      )}
    </PageContainer>
  )
}

function NavBarMenuDesktop() {
  const { isPending, isAuthenticated, logout } = useAuthContext()
  const router = useRouter()
  const loginURL = getLoginURL(router)
  const breakpoints = ['none', 'none', 'none', 'inherit']

  return (
    <Flex align="center" flex="none" pl={8} display={breakpoints}>
      <NextLink href="/" passHref>
        <Link color="chula.White" fontSize="16px" fontFamily="Sarabun" mr={6}>
          หน้าแรก
        </Link>
      </NextLink>
      <NextLink href="/request" passHref>
        <Link color="chula.White" fontSize="16px" fontFamily="Sarabun" mr={6}>
          เปิดประเด็น
        </Link>
      </NextLink>
      <NextLink href="/terms" passHref>
        <Link color="chula.White" fontSize="16px" fontFamily="Sarabun" mr={6}>
          ข้อตกลง
        </Link>
      </NextLink>
      {isPending ? (
        <NavBarButton>
          <CustomSpinner size="sm" thickness="2px" />
        </NavBarButton>
      ) : !isAuthenticated ? (
        <NextLink href={loginURL} passHref>
          <NavBarButton as="a">เข้าสู่ระบบ</NavBarButton>
        </NextLink>
      ) : (
        <NavBarButton
          onClick={() => logout().then(() => window.location.reload())}
        >
          ออกจากระบบ
        </NavBarButton>
      )}
    </Flex>
  )
}

export default function NavBar() {
  const pageMeta = usePageMeta()
  const { isOpen, onClose, onToggle } = useDisclosure()
  const theme = useTheme()
  const router = useRouter()
  const hasExtraItems = !!pageMeta.navBarItems

  const handleNavBarClick = useCallback(
    (e) => {
      if (e.target.nodeName === 'DIV') {
        onClose()
      }
    },
    [onClose]
  )

  useEffect(() => {
    onClose()
  }, [onClose, router.pathname])

  return (
    <NavBarContext.Provider value={{ onClose }}>
      <NavBarBox bg="transparent" />
      <FixedNavBarBox
        h={isOpen ? ['100%', '100%', '100%', '228px'] : 12}
        alignItems="start"
        transition="height 0.3s"
        flexDir="column"
        bg={transparentize('chula.PrimaryPink', 0.9)(theme)}
        style={{
          backdropFilter: 'blur(4px)',
          WebkitBackdropFilter: 'blur(4px)',
          overflowY: 'hidden',
        }}
        onClick={handleNavBarClick}
      >
        <SafeArea width="100%">
          <PageContainer>
            <NavBarBox bg={null}>
              <NextLink href="/" passHref>
                <Link marginLeft={-3} paddingX={2} minWidth="52px">
                  <img style={{ height: 36 }} src={logo} alt="หน้าหลัก" />
                </Link>
              </NextLink>
              <NextLink href={pageMeta.titleHref || '/'} passHref>
                <Link
                  fontFamily="CHULALONGKORN"
                  fontSize="2xl"
                  color="white"
                  whiteSpace="nowrap"
                  overflow="hidden"
                  minWidth="0"
                  mr={1}
                  style={{ textOverflow: 'ellipsis' }}
                >
                  {pageMeta.title || pageMeta.appTitle}
                </Link>
              </NextLink>
              <Box flex={1} />
              <NavBarMenuDesktop />
              {pageMeta.searchButton}
              <ToggleExpand onClick={onToggle} showOnDesktop={hasExtraItems} />
            </NavBarBox>
          </PageContainer>
          <NavBarMenuMobile />
        </SafeArea>
      </FixedNavBarBox>
    </NavBarContext.Provider>
  )
}
