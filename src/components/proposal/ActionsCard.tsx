import { Action } from '@/utils/api/proposal'
import { Box, Icon, Link } from '@chakra-ui/core'
import styled from '@emotion/styled'
import { format, parseISO } from 'date-fns'
import React from 'react'
import Linkify from 'react-linkify'

interface ActionsCardProps {
  title: React.ReactNode
  actions: Action[]
}

const Table = styled.table`
  font-family: Sarabun;
  font-size: 14px;

  td {
    vertical-align: baseline;
  }

  td:first-of-type {
    padding-right: 8px;
    width: 75px;
  }

  td:last-of-type {
    border-left: 1px solid ${({ theme }: any) => theme.colors.chula.PrimaryPink};
    padding-left: 8px;
    padding-right: 16px;
    max-width: calc(100% - 92px);
    word-wrap: break-word;

    a {
      word-break: break-all;
    }
  }
`

const CustomLinkInText = (
  decoratedHref: string,
  decoratedText: string,
  key: number
) => (
  <Link
    key={key}
    href={decoratedHref}
    color="chula.PrimaryPink"
    target="_blank"
    rel="noreferrer"
  >
    {decoratedText}
    <Icon name="external-link" mx="4px" mb="4px" />
  </Link>
)

const ActionsCard = React.forwardRef<HTMLDivElement, ActionsCardProps>(
  ({ title, actions }, ref) => {
    return (
      <Box
        ref={ref}
        borderColor="chula.PrimaryPink"
        borderWidth={1}
        borderRadius={5}
        boxSizing="border-box"
        overflow="hidden"
      >
        <Box bg="chula.PrimaryPink" padding={2}>
          {title}
        </Box>
        <Box margin={2}>
          <Table>
            <tbody>
              {actions.map((action: Action) => {
                const date = parseISO(action.createdAt)
                return (
                  <tr key={action.detail}>
                    <td>{format(date, 'd/L/yyyy')}</td>
                    <td>
                      <Linkify componentDecorator={CustomLinkInText}>
                        {action.detail}
                      </Linkify>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </Table>
        </Box>
      </Box>
    )
  }
)

export { ActionsCard }
