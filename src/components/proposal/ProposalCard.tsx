import { Box, Link, Stack } from '@chakra-ui/core'
import NextLink from 'next/link'
import { Proposal } from '@/utils/api/proposal'
import { APP_HOST } from '@/utils/config'
import { Card } from '@/components/common/Card'
import { CustomButton } from '@/components/common/CustomButton'
import { Share } from '@/components/common/Share'

const ProposalCard: React.FC<{ proposal: Proposal }> = ({ proposal }) => {
  return (
    <Card borderColor="chula.SecondaryPink" paddingX="20px">
      <NextLink href={`/${proposal._id}`} passHref>
        <Link fontFamily="ChulaCharasNew" fontSize="22px">
          <b>{proposal.name}</b>
        </Link>
      </NextLink>
      <Box
        fontFamily="Sarabun"
        fontSize="14px"
        color="chula.Gray"
        style={{ textIndent: '2.5rem' }}
        dangerouslySetInnerHTML={{ __html: proposal.description }}
      />
      <Stack spacing={2} isInline marginTop={2} justifyContent="center">
        <Box>
          <NextLink href={`/${proposal._id}`} passHref>
            <CustomButton variantColor="PrimaryPink" as="a">
              ร่วมส่งเสียง
            </CustomButton>
          </NextLink>
        </Box>
        <Share
          title={proposal.name}
          url={`${APP_HOST}/${proposal._id}`}
          shareButton={
            <CustomButton
              variantColor="Gold"
              variant="outline"
              color="chula.Copper"
              borderColor="chula.Gold"
              fontWeight="bold"
            >
              ร่วมส่งต่อ
            </CustomButton>
          }
        />
      </Stack>
    </Card>
  )
}

export { ProposalCard }
