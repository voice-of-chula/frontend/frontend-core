import React from 'react'
import { useDeviceDetect, useShare, useCustomToast } from '@/utils'
import {
  Box,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  useClipboard,
} from '@chakra-ui/core'
import { MenuItemLink } from './MenuItemLink'
import { FiFacebook, FiLink, FiTwitter } from 'react-icons/fi'

interface ShareProps {
  shareButton: React.ReactElement
  title: string
  url: string
}

const Share: React.FC<ShareProps> = (props) => {
  const { title, url: initialURL, shareButton } = props
  const timeStamp = Math.floor(Date.now() / 1000) * 1000
  const url = `${initialURL}?at=${timeStamp}`

  const { canShare, share } = useShare(title, url)
  const { isMobile } = useDeviceDetect()

  const { hasCopied, onCopy } = useClipboard(url)
  const { successToast } = useCustomToast()
  if (hasCopied) {
    successToast({
      title: 'คัดลอกลิงก์แล้ว',
    })
  }

  if (canShare && isMobile) {
    return React.cloneElement(shareButton, { onClick: share })
  }
  return (
    <Menu>
      <MenuButton as="a" children={shareButton} />
      <MenuList>
        <MenuItemLink
          href={`https://www.facebook.com/sharer/sharer.php?u=${url}`}
          target="_blank"
        >
          <Box ml="-0.25rem" mr="0.5rem">
            <FiFacebook />
          </Box>
          แชร์ไปยัง Facebook
        </MenuItemLink>
        <MenuItemLink
          href={`http://www.twitter.com/share?url=${url}`}
          target="_blank"
        >
          <Box ml="-0.25rem" mr="0.5rem">
            <FiTwitter />
          </Box>
          แชร์ไปยัง Twitter
        </MenuItemLink>
        <MenuItem onClick={onCopy}>
          <Box ml="-0.25rem" mr="0.5rem">
            <FiLink />
          </Box>
          คัดลอกลิงก์ไปยังคลิปบอร์ด
        </MenuItem>
      </MenuList>
    </Menu>
  )
}

export { Share }
