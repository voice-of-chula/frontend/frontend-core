import { Box, BoxProps } from '@chakra-ui/core'

const PinkCard: React.FC<BoxProps & { children?: React.ReactNode }> = (
  props
) => {
  return (
    <Box
      bg="chula.SecondaryPink"
      marginX="32px"
      marginY="60px"
      padding={5}
      borderRadius={9}
      {...props}
    />
  )
}

export { PinkCard }
