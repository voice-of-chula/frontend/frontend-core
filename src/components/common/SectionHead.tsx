import { Box, Text, Flex, BoxProps } from '@chakra-ui/core'

const SectionHead: React.FC<
  BoxProps & { title: string; subtitle?: string }
> = ({ title, subtitle, ...props }) => {
  return (
    <Box marginY={14} {...props}>
      <Flex flexWrap="wrap" alignItems="baseline">
        <Text
          pr={2}
          fontFamily="CHULALONGKORN"
          fontSize="2xl"
          color="chula.PrimaryPink"
        >
          <b>{title}</b>
        </Text>
        {subtitle && (
          <Text
            flex="1 1 auto"
            fontFamily="CHULALONGKORN"
            fontSize="18px"
            color="chula.PrimaryPink"
          >
            <b>{subtitle}</b>
          </Text>
        )}
      </Flex>
      <Box w="100%" h="1px" bg="chula.Gold" />
    </Box>
  )
}

export { SectionHead }
