import { BoxProps, ModalOverlay } from '@chakra-ui/core'

const CustomModalOverlay: React.FC<BoxProps> = (props) => {
  return (
    <ModalOverlay
      style={{
        backdropFilter: 'blur(16px)',
        WebkitBackdropFilter: 'blur(16px)',
      }}
      {...props}
    />
  )
}

export { CustomModalOverlay }
