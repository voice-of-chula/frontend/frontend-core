import styled from '@emotion/styled'
import { Box } from '@chakra-ui/core'

const StickyBox = styled(Box)`
  position: sticky;
`

export { StickyBox }
