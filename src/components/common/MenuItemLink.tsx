import { MenuItem, MenuItemProps } from '@chakra-ui/core'
import { AnchorHTMLAttributes } from 'react'

type MenuItemLinkProps = Omit<MenuItemProps, 'as'> &
  AnchorHTMLAttributes<HTMLAnchorElement>

const MenuItemLink: React.FC<MenuItemLinkProps> = (props) => (
  <MenuItem {...props} as="a" />
)

export { MenuItemLink }
