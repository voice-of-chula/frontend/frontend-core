import { Box, useTheme, BoxProps } from '@chakra-ui/core'
import { transparentize } from '@chakra-ui/theme-tools'

const Card: React.FC<
  BoxProps & { borderColor: string; children?: React.ReactNode }
> = ({ borderColor, ...props }) => {
  const theme = useTheme()
  return (
    <Box
      borderColor={borderColor}
      borderWidth={1}
      borderRadius={10}
      padding={3}
      marginBottom="14px"
      boxShadow={`0px 4px 4px ${transparentize(borderColor, 0.25)(theme)}`}
      {...props}
    />
  )
}

export { Card }
