import Head from 'next/head'
import { usePageMeta } from '@/utils/pageMeta'

const autocompleteProperties: Record<string, string[]> = {
  name: ['og:title', 'twitter:title', 'og:image:alt'],
  description: ['og:description', 'twitter:description'],
  image: ['og:image', 'og:image:secure_url', 'twitter:image'],
}

export default function Metas() {
  const pageMeta = usePageMeta()
  const meta = pageMeta.meta

  const duplicated = Object.keys(autocompleteProperties).reduce(
    (target, primer) => {
      const equalProperties = autocompleteProperties[primer]
      return meta[primer]
        ? {
            ...target,
            ...equalProperties.reduce(
              (target, name) => ({ ...target, [name]: meta[primer] }),
              {}
            ),
          }
        : target
    },
    {}
  )

  const finalMeta = { ...duplicated, ...meta }
  return (
    <Head>
      <title>
        {pageMeta.pageTitle
          ? `${pageMeta.pageTitle} | ${pageMeta.appTitle}`
          : pageMeta.appTitle}
      </title>
      {Object.keys(finalMeta).map((name) =>
        /^og/.test(name) ? (
          <meta key={name} property={name} content={finalMeta[name]} />
        ) : (
          <meta key={name} name={name} content={finalMeta[name]} />
        )
      )}
    </Head>
  )
}
