import React from 'react'
import { Button, ButtonProps } from '@chakra-ui/core'

const CustomButton = React.forwardRef<HTMLElement, ButtonProps>(
  (props, ref) => {
    return (
      <Button
        ref={ref}
        fontFamily="ChulaCharasNew"
        fontWeight="normal"
        fontSize="24px"
        {...props}
      />
    )
  }
)

export { CustomButton }
