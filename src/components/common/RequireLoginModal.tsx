import {
  useDisclosure,
  Button,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
} from '@chakra-ui/core'
import { UseDisclosureReturn } from '@chakra-ui/core/dist/useDisclosure'
import { useCallback } from 'react'
import { useAuthContext } from '@/providers/AuthProvider'
import { useRouter } from 'next/router'
import { getLoginURL } from '@/utils'
import Link from 'next/link'
import { CustomModalOverlay } from './CustomModalOverlay'
import { CustomModal } from './CustomModal'

const RequireLoginModal: React.FC<
  { loginReason: React.ReactNode; loginURL?: string } & UseDisclosureReturn
> = ({ loginReason, loginURL: overrideLoginURL, isOpen, onClose }) => {
  const router = useRouter()
  const loginURL = overrideLoginURL || getLoginURL(router)

  return (
    <>
      <CustomModal isOpen={isOpen} onClose={onClose}>
        <CustomModalOverlay />
        <ModalContent>
          <ModalHeader fontFamily="ChulaCharasNew">
            ยังไม่ได้เข้าสู่ระบบ
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody fontFamily="Sarabun">
            กรุณาเข้าสู่ระบบเพื่อ{loginReason}
          </ModalBody>

          <ModalFooter>
            <Button
              fontFamily="ChulaCharasNew"
              variant="ghost"
              mr={3}
              onClick={onClose}
            >
              ยกเลิก
            </Button>
            <Link href={loginURL}>
              <Button
                fontFamily="ChulaCharasNew"
                variantColor="PrimaryPink"
                onClick={onClose}
              >
                เข้าสู่ระบบ
              </Button>
            </Link>
          </ModalFooter>
        </ModalContent>
      </CustomModal>
    </>
  )
}

type Arr = readonly unknown[]

export interface RequireLoginResult<R> {
  success: boolean
  result?: R
}

export interface RequireLoginReturn<T extends Arr, R> {
  func: (...args: T) => RequireLoginResult<R>
  modal: React.ReactNode
}

export function useRequireLogin<T extends Arr, R>(
  loginReason: React.ReactNode,
  func: (...args: T) => R,
  loginURL?: string
): RequireLoginReturn<T, R> {
  const { isAuthenticated } = useAuthContext()
  const useDisclosureReturn = useDisclosure()
  const composed = useCallback(
    (...args: T): RequireLoginResult<R> => {
      if (isAuthenticated) {
        return {
          success: true,
          result: func(...args),
        }
      } else {
        useDisclosureReturn.onOpen()
        return {
          success: false,
        }
      }
    },
    [func, isAuthenticated, useDisclosureReturn.onOpen]
  )

  return {
    func: composed,
    modal: (
      <RequireLoginModal
        loginReason={loginReason}
        loginURL={loginURL}
        {...useDisclosureReturn}
      />
    ),
  }
}
