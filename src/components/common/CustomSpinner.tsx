import { Spinner, SpinnerProps, Flex } from '@chakra-ui/core'

const CustomSpinner: React.FC<SpinnerProps> = (props) => {
  return (
    <Flex flex={1} alignItems="center" flexDirection="column">
      <Spinner color="chula.PrimaryPink" size="xl" thickness="4px" {...props} />
    </Flex>
  )
}

export { CustomSpinner }
