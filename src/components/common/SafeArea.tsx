import styled from '@emotion/styled'
import { Box } from '@chakra-ui/core'

const SafeArea = styled(Box)`
  padding-left: env(safe-area-inset-left);
  padding-right: env(safe-area-inset-right);
`

export { SafeArea }
