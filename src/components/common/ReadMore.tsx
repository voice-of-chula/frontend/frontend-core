import React, { useState, useCallback } from 'react'
import {
  Box,
  Stack,
  Button,
  Collapse,
  BoxProps,
  PseudoBox,
} from '@chakra-ui/core'
import styled from '@emotion/styled'
import Measure, { ContentRect } from 'react-measure'

export interface ReadMoreButtonProps {
  isOpen: boolean
  onToggle: () => void
}

interface ReadMoreProps {
  children: React.ReactElement<React.RefAttributes<any>>
  collapsedHeight: number
  collapseThreshold: number
  canCollapse?: boolean
  gradient?: boolean
  readMoreButton?: React.FunctionComponent<ReadMoreButtonProps>
  clickToToggle?: boolean
}

interface CollapseState {
  defaultOpen: boolean
  isOpen: boolean
  isInteracted: boolean
}

const CollapseContainer = styled(Stack)<{ noAnimation: boolean }>`
  div {
    width: 100%;
    ${({ noAnimation }) => noAnimation && `transition: none;`}
  }
`

const defaultReadMoreButton: React.FunctionComponent<ReadMoreButtonProps> = ({
  onToggle,
}) => (
  <Button
    width="100%"
    onClick={onToggle}
    variant="link"
    fontFamily="ChulaCharasNew"
    fontSize="18px"
    paddingY={2}
    variantColor="Gold"
    color="chula.Copper"
  >
    อ่านเพิ่มเติม
  </Button>
)

const ReadMore: React.FC<BoxProps & ReadMoreProps> = (props) => {
  const {
    children,
    collapsedHeight,
    collapseThreshold,
    canCollapse = false,
    gradient = true,
    readMoreButton = defaultReadMoreButton,
    clickToToggle = false,
    ...boxProps
  } = props
  const [collapseState, setCollapseState] = useState<CollapseState>({
    defaultOpen: false,
    isOpen: false,
    isInteracted: false,
  })
  const { defaultOpen, isOpen, isInteracted } = collapseState
  const onToggle = useCallback(() => {
    setCollapseState((oldState) => ({
      defaultOpen: oldState.defaultOpen,
      isOpen: !oldState.isOpen,
      isInteracted: true,
    }))
  }, [])

  const onResize = useCallback((contentRect: ContentRect) => {
    setCollapseState((oldState) => {
      if (oldState.isInteracted) {
        return oldState
      } else {
        const open = contentRect.bounds.height < collapseThreshold
        return {
          defaultOpen: open,
          isOpen: open,
          isInteracted: false,
        }
      }
    })
  }, [])

  return (
    <Box position="relative" {...boxProps}>
      <PseudoBox
        _before={{
          width: '100%',
          height: collapsedHeight,
          position: 'absolute',
          ...(gradient &&
            !isOpen && {
              content: `""`,
              backgroundImage: 'linear-gradient(rgba(255, 255, 255, 0), white)',
            }),
        }}
      >
        <CollapseContainer alignItems="center" noAnimation={!isInteracted}>
          <Collapse
            isOpen={defaultOpen || isOpen}
            startingHeight={collapsedHeight}
          >
            <Measure bounds onResize={onResize}>
              {({ measureRef }) =>
                React.cloneElement(children, {
                  ref: measureRef,
                  onClick: clickToToggle ? onToggle : undefined,
                })
              }
            </Measure>
          </Collapse>
          {!defaultOpen && (
            <Collapse isOpen={canCollapse || !isOpen}>
              {readMoreButton({ isOpen, onToggle })}
            </Collapse>
          )}
        </CollapseContainer>
      </PseudoBox>
    </Box>
  )
}

export { ReadMore }
