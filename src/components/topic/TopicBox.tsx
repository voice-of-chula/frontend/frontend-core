import { Flex, FlexProps, Link } from '@chakra-ui/core'
import { useProposalContext } from '@/providers/ProposalProvider'
import NextLink from 'next/link'
import { topicPagePath } from '@/pages/[proposalId]/topic/[topic]'

const TopicBox: React.FC<{ topic: string }> = ({ topic }) => {
  const { proposalId } = useProposalContext()

  return (
    <NextLink href={topicPagePath(proposalId, topic)} passHref>
      <Link
        mr="8px"
        mb="8px"
        px="8px"
        borderWidth="1px"
        borderColor="chula.PrimaryPink"
        rounded="5px"
        fontFamily="Sarabun"
        fontSize="14px"
        color="chula.PrimaryPink"
        textDecoration="none"
      >
        {topic}
      </Link>
    </NextLink>
  )
}

const TopicBoxGroup: React.FC<FlexProps & { topics: string[] }> = ({
  topics,
  ...rest
}) => {
  return (
    <Flex flexWrap="wrap" {...rest}>
      {topics.map((topic) => (
        <TopicBox key={topic} topic={topic} />
      ))}
    </Flex>
  )
}

export { TopicBox, TopicBoxGroup }
