import React from 'react'
import { Divider, Flex, Icon, Text } from '@chakra-ui/core'
import Link from 'next/link'
import { Topic } from '@/utils/api/proposal'
import { useProposalContext } from '@/providers/ProposalProvider'

export interface TopicItemProps {
  topic: Topic
}

export default function TopicItem(props: TopicItemProps) {
  const { topic } = props
  const { proposalId: _id } = useProposalContext()
  const topicUrl = `/${_id}/topic/${topic.name}`
  return (
    <>
      <Link href={topicUrl} passHref>
        <a>
          <Flex
            flexDir="row"
            justifyContent="space-between"
            alignItems="center"
            h="50px"
            cursor="pointer"
          >
            <Text
              marginLeft={4}
              fontFamily="ChulaCharasNew"
              fontSize="18px"
              as="b"
            >
              {topic.name}
            </Text>
            <Text
              fontFamily="ChulaCharasNew"
              fontSize="16px"
              textDecoration="none"
            >
              {topic.count} เสียง
              <Icon mx={4} name="chevron-right" color="chula.Gray" />
            </Text>
          </Flex>
        </a>
      </Link>
      <Divider orientation="horizontal" my={0} />
    </>
  )
}
