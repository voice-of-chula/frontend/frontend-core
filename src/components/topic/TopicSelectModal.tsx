import {
  InputGroup,
  InputLeftElement,
  ModalContent,
  Button,
} from '@chakra-ui/core'
import { useDisclosure, Input, Box, Flex, Icon } from '@chakra-ui/core'
import React, { useCallback, useEffect, useState } from 'react'
import { Topic } from '@/utils/api/proposal'
import { CustomModal } from '@/components/common/CustomModal'
import { CustomModalOverlay } from '@/components/common/CustomModalOverlay'
import { SectionHead } from '@/components/common/SectionHead'
import TopicItem from './TopicItem'

interface TopicSelectProps {
  topics: Topic[]
  isProposalPage?: boolean
}

export default function TopicSelectModal(props: TopicSelectProps) {
  const { topics, isProposalPage } = props
  const { isOpen, onOpen, onClose } = useDisclosure()
  const [searchResult, setResult] = useState(topics)
  const [searchText, setSearchText] = useState('')

  const handleChangeSearchText = useCallback((event) => {
    const value: string = event.target.value
    setSearchText(value)
  }, [])

  useEffect(() => {
    if (searchText.trim().length === 0) {
      setResult(topics)
    } else {
      const result = topics.filter(
        (topic) => topic.name.indexOf(searchText) !== -1
      )
      setResult(result)
    }
  }, [searchText])

  return (
    <>
      {isProposalPage ? (
        <Button
          fontSize="18px"
          fontFamily="ChulaCharasNew"
          textAlign="center"
          color="chula.Copper"
          onClick={onOpen}
          cursor="pointer"
          py="20px"
          variant="link"
        >
          <b>{'ดูทั้งหมด >>'}</b>
        </Button>
      ) : (
        <Icon
          size="24px"
          color="chula.White"
          name="search"
          onClick={onOpen}
          cursor="pointer"
        />
      )}
      <CustomModal closeOnOverlayClick={true} isOpen={isOpen} onClose={onClose}>
        <CustomModalOverlay />
        <ModalContent
          justifyContent="center-top"
          w="344px"
          h="675px"
          border="1px"
          borderColor="chula.PrimaryPink"
          rounded="5px"
        >
          <Box px={4}>
            <SectionHead title="หัวข้อทั้งหมด" />
            <Flex direction="column">
              <InputGroup>
                <InputLeftElement
                  children={
                    <Icon
                      h="28px"
                      mr="0px"
                      pr="0px"
                      mb="10px"
                      size="16px"
                      name="search"
                      color="chula.PrimaryPink"
                    />
                  }
                />
                <Input
                  h="28px"
                  mb="8px"
                  fontFamily="Sarabun"
                  fontSize="16px"
                  placeholder="ค้นหาหัวข้อ"
                  borderRadius="15px"
                  border="1px"
                  borderColor="chula.PrimaryPink"
                  focusBorderColor="chula.PrimaryPink"
                  onChange={handleChangeSearchText}
                />
              </InputGroup>
            </Flex>

            <Box
              paddingTop="5px"
              paddingX="10px"
              borderColor="chula.PrimaryPink"
              rounded="15px"
              h="565px"
              overflowY="scroll"
            >
              {searchResult.map((topic: Topic) => (
                <TopicItem key={topic.name} topic={topic} />
              ))}
            </Box>
          </Box>
        </ModalContent>
      </CustomModal>
    </>
  )
}
