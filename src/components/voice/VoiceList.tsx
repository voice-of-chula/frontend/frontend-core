import { LoadAfter, useLoadMore } from '@/utils'
import { VoiceDto, DEFAULT_VOICE_COUNT } from '@/utils/api/voice'
import InfiniteScroll from 'react-infinite-scroller'
import { VoiceCard } from './VoiceCard'
import { CustomSpinner } from '@/components/common/CustomSpinner'
import { QueryKey } from 'react-query'

interface VoiceListProps {
  queryKey: QueryKey
  loadAfter: LoadAfter<VoiceDto>
  initialVoices: VoiceDto[]
  voiceFilter?: (voice: VoiceDto) => boolean
}

const VoiceList: React.FC<VoiceListProps> = ({
  queryKey,
  loadAfter,
  initialVoices,
  voiceFilter,
}) => {
  const { data, hasMore, loadMore, loading } = useLoadMore(
    queryKey,
    loadAfter,
    DEFAULT_VOICE_COUNT,
    initialVoices
  )
  return (
    <>
      <InfiniteScroll
        pageStart={0}
        loadMore={loadMore}
        hasMore={hasMore}
        threshold={1000}
      >
        {data.map((voice) => {
          if (voiceFilter && !voiceFilter(voice)) {
            return null
          }
          return <VoiceCard key={voice._id} voice={voice} />
        })}
      </InfiniteScroll>
      {loading && <CustomSpinner mt={4} />}
    </>
  )
}

export { VoiceList }
