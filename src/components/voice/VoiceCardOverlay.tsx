import {
  Flex,
  theme,
  Text,
  Scale as ChakraScale,
  IScale,
} from '@chakra-ui/core'
import { transparentize } from '@chakra-ui/theme-tools'
import React, { useCallback } from 'react'
import { CustomButton } from '@/components/common/CustomButton'

const Scale: React.FC<IScale> = ChakraScale

interface VoiceCardOverlayProps {
  isVisible: boolean
  onSubmit: () => void
  onCancel: () => void
  confirmText: string
}

export default function VoiceCardOverlay(props: VoiceCardOverlayProps) {
  const { isVisible, onSubmit, onCancel, confirmText } = props

  const handleClick = useCallback(
    (e) => {
      if (e.target.nodeName === 'DIV') {
        onCancel()
      }
    },
    [onCancel]
  )

  return (
    <Scale in={isVisible}>
      {(styles: any) => (
        <Flex
          position="absolute"
          width="100%"
          height="100%"
          flex={1}
          top={0}
          zIndex={5}
          borderRadius={10}
          backgroundColor={transparentize(`#000000`, 0.75)(theme)}
          style={{
            backdropFilter: 'blur(5px)',
            WebkitBackdropFilter: 'blur(5px)',
          }}
          opacity={styles.opacity}
          onClick={handleClick}
        >
          <Flex
            px={2}
            justifyContent="center"
            alignItems="center"
            flexDirection="column"
            flex={1}
            {...styles}
          >
            <Text
              fontFamily="Sarabun"
              fontWeight="100"
              fontSize="20px"
              color="chula.White"
            >
              {confirmText}
            </Text>
            <Flex flexDirection="row" mt={18}>
              <CustomButton
                width={145}
                height={45}
                variantColor="PrimaryPink"
                fontSize="20px"
                fontWeight="bold"
                onClick={onSubmit}
              >
                ยืนยัน
              </CustomButton>
              <CustomButton
                ml={8}
                width={145}
                height={45}
                variant="outline"
                variantColor="Gold"
                backgroundColor="White"
                color="chula.Copper"
                borderColor="chula.Copper"
                fontSize="20px"
                fontWeight="bold"
                onClick={onCancel}
              >
                ยกเลิก
              </CustomButton>
            </Flex>
          </Flex>
        </Flex>
      )}
    </Scale>
  )
}
