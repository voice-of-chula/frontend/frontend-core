import { VoiceDto } from '@/utils/api/voice'
import { Text } from '@chakra-ui/core'
import styled from '@emotion/styled'
import { forwardRef, Fragment, memo } from 'react'

export interface VoiceDetailProps {
  voice: VoiceDto
}

const VoiceDetailLayout = styled(Text)`
  p + p {
    margin-top: 8px;
  }
`

export const VoiceDetail = memo(
  forwardRef<HTMLDivElement, VoiceDetailProps>(({ voice }, ref) => {
    const { detail } = voice
    const paragraphs = detail.split(/\n\n+\b/g)
    return (
      <VoiceDetailLayout
        ref={ref}
        as="div"
        fontFamily="Sarabun"
        fontSize="16px"
        color="black"
        overflowY="hidden"
      >
        {paragraphs.map((paragraph, idx) => (
          <DetailParagraph key={idx} paragraph={paragraph} />
        ))}
      </VoiceDetailLayout>
    )
  })
)

interface DetailParagraphProps {
  paragraph: string
}

function DetailParagraph({ paragraph }: DetailParagraphProps) {
  const elements = []
  const lines = paragraph.split(/\n\b/g)
  lines.forEach((line, idx) => {
    elements.push(<Fragment key={idx * 2}>{line}</Fragment>)
    elements.push(<br key={idx * 2 + 1} />)
  })
  return <p>{elements}</p>
}
