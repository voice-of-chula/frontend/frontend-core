import { Button, IconButton, Input, Text, Stack } from '@chakra-ui/core'
import Link from 'next/link'
import { useCallback, useState } from 'react'
import { BsChatDotsFill } from 'react-icons/bs'
import { useAuthContext } from '@/providers/AuthProvider'
import { LoadMoreResult } from '@/utils'
import { ReplyDto } from '@/utils/api/voice'
import { ReplyRow } from './ReplyRow'

interface ReplyListProps {
  replies: ReplyDto[]
  loadMoreReplies: LoadMoreResult<ReplyDto>
}

const ReplyList: React.FC<ReplyListProps> = ({ replies, loadMoreReplies }) => {
  const { hasMore, loadMore, loading } = loadMoreReplies
  return (
    <>
      {loading && (
        <Text mt="12px" fontFamily="ChulaCharasNew" fontSize="14px">
          กำลังโหลด...
        </Text>
      )}
      {hasMore && !loading && (
        <Button
          width="100%"
          mt="12px"
          fontFamily="ChulaCharasNew"
          fontSize="14px"
          justifyContent="start"
          variant="link"
          variantColor="PrimaryPink"
          onClick={loadMore}
        >
          ดูความคิดเห็นก่อนหน้า
        </Button>
      )}
      {replies.map((reply) => (
        <ReplyRow key={reply._id} reply={reply} />
      ))}
    </>
  )
}

interface ReplyBoxProps {
  loginURL: string
  onSubmit: (detail: string) => void
}

const ReplyBox: React.FC<ReplyBoxProps> = ({ loginURL, onSubmit }) => {
  const [detail, setDetail] = useState('')
  const { isAuthenticated } = useAuthContext()

  const handleChange = useCallback((e) => {
    setDetail(e.target.value)
  }, [])

  const handleSubmit = useCallback(
    (e) => {
      e.preventDefault()
      if (detail.trim().length == 0) return
      setDetail('')
      onSubmit(detail)
    },
    [detail, onSubmit]
  )

  if (!isAuthenticated) {
    return (
      <Link href={loginURL} passHref>
        <Button fontFamily="ChulaCharasNew" variantColor="PrimaryPink" as="a">
          เข้าสู่ระบบเพื่อตอบกลับ
        </Button>
      </Link>
    )
  }

  return (
    <form onSubmit={handleSubmit}>
      <Stack isInline>
        <Input
          fontFamily="Sarabun"
          fontSize="12px"
          placeholder="แสดงความคิดเห็น"
          border="1px"
          borderColor="chula.SecondaryPink"
          borderRadius="8px"
          focusBorderColor="chula.PrimaryPink"
          value={detail}
          onChange={handleChange}
          size="lg"
        />
        <IconButton
          type="submit"
          backgroundColor="chula.SecondaryPink"
          borderRadius="8px"
          color="chula.Gray"
          size="lg"
          icon={BsChatDotsFill}
          aria-label="Reply to voice"
        />
      </Stack>
    </form>
  )
}

export { ReplyList, ReplyBox }
