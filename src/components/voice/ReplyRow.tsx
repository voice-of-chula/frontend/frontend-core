import { ReplyDto } from '@/utils/api/voice'
import { Text, Box, Skeleton } from '@chakra-ui/core'
import { useOwnerInfo } from '@/utils'

interface ReplyRowProps {
  reply: ReplyDto
}

const ReplyRow: React.FC<ReplyRowProps> = ({ reply }) => {
  const { owner, detail, pendingId, error } = reply
  const ownerInfo = useOwnerInfo(owner)

  return (
    <Box py={2}>
      <Skeleton display="inline-block" height="18px" isLoaded={!pendingId}>
        <Text
          display="inline-block"
          fontFamily="Sarabun"
          fontWeight="100"
          fontSize="12px"
          color="Gray.200"
        >
          นิสิต{ownerInfo}
        </Text>
        {error && (
          <Text
            display="inline-block"
            fontFamily="Sarabun"
            fontSize="12px"
            color="red.500"
            pl={1}
          >
            - Failed to reply
          </Text>
        )}
      </Skeleton>
      <Box ml={2}>
        <Skeleton display="inline-block" isLoaded={!pendingId}>
          <Text fontFamily="Sarabun" fontSize="16px" color="black">
            {detail}
          </Text>
        </Skeleton>
      </Box>
    </Box>
  )
}

export { ReplyRow }
