import {
  Box,
  BoxProps,
  Button,
  Collapse,
  Divider,
  Flex,
  IconButton,
  PseudoBox,
  Text,
  useDisclosure,
  useTheme,
} from '@chakra-ui/core'
import { transparentize } from '@chakra-ui/theme-tools'
import NextLink from 'next/link'
import { useRouter } from 'next/router'
import React, { useMemo, useState, useCallback } from 'react'
import {
  AiFillLike,
  AiOutlineLike,
  AiFillDislike,
  AiOutlineDislike,
} from 'react-icons/ai'
import { FiFlag as reportIcon, FiShare2 as shareIcon } from 'react-icons/fi'
import { MdChatBubble, MdChatBubbleOutline } from 'react-icons/md'
import { useProposalContext } from '@/providers/ProposalProvider'
import {
  facultyNames,
  useFormatTime,
  useOwnerInfo,
  useParseOuid,
  useRelativeTime,
  useToastFeedback,
} from '@/utils'
import { deleteVoice, VoiceDto } from '@/utils/api/voice'
import { useVoice } from '@/utils/voiceModel'
import { ReadMore, ReadMoreButtonProps } from '../common/ReadMore'
import { ReplyBox, ReplyList } from './ReplyList'
import { Share } from '@/components/common/Share'
import { TopicBoxGroup } from '../topic/TopicBox'
import { RenderOnIntersect } from '@/components/common/RenderOnIntersect'
import VoiceCardOverlay from './VoiceCardOverlay'
import { useRequireLogin } from '../common/RequireLoginModal'
import { VoiceDetail } from './VoiceDetail'

interface VoiceCardProps {
  voice: VoiceDto
}

const seeMoreButton: React.FunctionComponent<ReadMoreButtonProps> = ({
  isOpen,
  onToggle,
}) => (
  <Button
    width="100%"
    fontFamily="Sarabun"
    fontWeight="normal"
    justifyContent="start"
    variant="link"
    color="Gray.50"
    onClick={onToggle}
    _focus={{}}
    _active={{}}
  >
    ... see {isOpen ? 'less' : 'more'}
  </Button>
)

const VoiceCard: React.FC<VoiceCardProps & BoxProps> = ({
  voice: voiceDto,
  ...props
}) => {
  const {
    voice,
    toggleUpvote,
    toggleDownvote,
    onReport,
    modals,
    voiceURL,
    replies,
    replyCount,
    submitReply,
    loadMoreReplies,
    loginURL,
  } = useVoice(voiceDto)
  const {
    _id,
    owner,
    topics,
    upvotes,
    downvotes,
    selfUpvoted,
    selfDownvoted,
    createdTime,
  } = voice
  const ownerInfo = useOwnerInfo(owner)
  const theme = useTheme()
  const relativeTime = useRelativeTime(createdTime)
  const formattedTime = useFormatTime(createdTime)
  const { proposalId } = useProposalContext()
  const { pathname } = useRouter()
  const isMyVoice = pathname.match('myVoice')

  const { facultyId } = useParseOuid(owner)
  const backgroundColor = facultyNames[facultyId]
    ? `Faculty.${facultyId}`
    : `chula.PrimaryPink`

  const shadowColor = useMemo(
    () => transparentize(backgroundColor, 0.25)(theme),
    []
  )

  const { isOpen: isRepliesOpen, onToggle: onToggleReplies } = useDisclosure(
    replyCount > 0
  )
  const { isOpen: isReportOpen, onToggle: onToggleReport } = useDisclosure()
  const { isOpen: isDeleteOpen, onToggle: onToggleDelete } = useDisclosure()
  const [isDeleted, setDeleted] = useState<boolean>(false)

  const { func: onToggleReportWithLogin, modal } = useRequireLogin(
    'รายงานเสียงนี้',
    onToggleReport,
    loginURL
  )

  const onConfirmReport = useCallback(async () => {
    onToggleReport()
    try {
      await onReport()
    } catch (e) {
      // do nothing
    }
  }, [onReport, onToggleReport])

  const deleteVoiceWithFeedback = useToastFeedback('ลบเสียง', deleteVoice)

  const onConfirmDelete = useCallback(async () => {
    onToggleDelete()
    try {
      await deleteVoiceWithFeedback(proposalId, _id)
      setDeleted(true)
    } catch (e) {
      // do nothing
    }
  }, [deleteVoiceWithFeedback])

  if (isDeleted) return null

  const realCard = (
    <Flex {...props} direction="column">
      <PseudoBox
        borderColor={backgroundColor}
        borderWidth="1px"
        borderRadius="10px"
        py={2}
        position="relative"
        marginBottom="24px"
        transition="box-shadow 0.2s ease-out"
        _hover={{
          boxShadow: `0px 4px 4px ${shadowColor}`,
        }}
      >
        <VoiceCardOverlay
          isVisible={isReportOpen}
          onSubmit={onConfirmReport}
          onCancel={onToggleReport}
          confirmText="คุณต้องการที่จะรายงานเสียงนี้?"
        />

        <VoiceCardOverlay
          isVisible={isDeleteOpen}
          onSubmit={onConfirmDelete}
          onCancel={onToggleDelete}
          confirmText="คุณต้องการที่จะลบเสียงนี้?"
        />

        {isMyVoice && (
          <IconButton
            size="sm"
            position="absolute"
            top={1}
            right={1}
            zIndex={2}
            aria-label="Delete voice"
            icon="small-close"
            isRound
            variantColor="PrimaryPink"
            onClick={onToggleDelete}
          />
        )}

        <Flex px={2}>
          <Flex direction="column" flex={1} maxWidth="calc(100% - 64px)">
            <NextLink href={voiceURL} passHref>
              <a>
                <Flex mb="8px">
                  <Text
                    flex="1 1 0px"
                    fontFamily="Sarabun"
                    fontWeight="100"
                    fontSize="12px"
                    color="Gray.200"
                  >
                    นิสิต{ownerInfo}
                  </Text>
                  <PseudoBox
                    fontFamily="Sarabun"
                    fontWeight="100"
                    fontSize="12px"
                    color="Gray.100"
                    title={formattedTime}
                    _hover={{
                      textDecoration: 'underline',
                    }}
                  >
                    {relativeTime}
                  </PseudoBox>
                </Flex>
              </a>
            </NextLink>
            <TopicBoxGroup topics={topics} />
            <ReadMore
              collapsedHeight={72}
              collapseThreshold={100}
              canCollapse
              gradient={false}
              readMoreButton={seeMoreButton}
              clickToToggle
            >
              <VoiceDetail voice={voice} />
            </ReadMore>
            <Flex>
              <Button
                height="2rem"
                leftIcon={isRepliesOpen ? MdChatBubble : MdChatBubbleOutline}
                variant="ghost"
                fontFamily="Sarabun"
                fontSize="14px"
                variantColor="PrimaryPink"
                onClick={onToggleReplies}
              >
                {replyCount} ความคิดเห็น
              </Button>
              <Share
                title="เสียงของนิสิต"
                url={voiceURL}
                shareButton={
                  <Button
                    height="2rem"
                    leftIcon={shareIcon}
                    variant="ghost"
                    fontFamily="Sarabun"
                    fontSize="14px"
                    variantColor="PrimaryPink"
                  >
                    แชร์
                  </Button>
                }
              />
              <Box flex={1} />
              <IconButton
                height="2rem"
                aria-label=""
                icon={reportIcon}
                variant="ghost"
                variantColor="PrimaryPink"
                color="Gray.200"
                onClick={onToggleReportWithLogin}
              />
            </Flex>
          </Flex>
          {modal}

          <Divider borderColor="gray.300" orientation="vertical" />

          <Flex direction="column" justify="center" align="center">
            <Button
              display="block"
              height="100%"
              variantColor="PrimaryPink"
              variant="ghost"
              onClick={toggleUpvote}
            >
              {selfUpvoted ? <AiFillLike /> : <AiOutlineLike />}
              <Text
                fontFamily="Sarabun"
                fontWeight="100"
                fontSize="14px"
                color="chula.PrimaryPink"
                mt={1}
              >
                {upvotes}
              </Text>
            </Button>
            <Button
              display="block"
              height="100%"
              variantColor="PrimaryPink"
              variant="ghost"
              onClick={toggleDownvote}
            >
              {selfDownvoted ? <AiFillDislike /> : <AiOutlineDislike />}
              <Text
                fontFamily="Sarabun"
                fontWeight="100"
                fontSize="14px"
                color="chula.PrimaryPink"
                mt={1}
              >
                {downvotes}
              </Text>
            </Button>
          </Flex>
        </Flex>
        <Collapse isOpen={isRepliesOpen} startingHeight={0}>
          <Divider borderColor="gray.300" mb={0} />
          <Box mx={2}>
            <ReplyList replies={replies} loadMoreReplies={loadMoreReplies} />
          </Box>
          <Box mx="10px" mt="10px" mb="2px">
            <ReplyBox loginURL={loginURL} onSubmit={submitReply} />
          </Box>
        </Collapse>
        {modals}
      </PseudoBox>
    </Flex>
  )

  return (
    <RenderOnIntersect id={`voice/${_id}`} initialHeight={227}>
      {realCard}
    </RenderOnIntersect>
  )
}

/* wrap it in a PureComponent to prevent re-rendering when parent renders */
class VoiceCardWrapper extends React.PureComponent<VoiceCardProps> {
  render() {
    return <VoiceCard voice={this.props.voice} />
  }
}

export { VoiceCardWrapper as VoiceCard }
