import React, { useState, Dispatch, SetStateAction, useEffect } from 'react'

import {
  ModalContent,
  useDisclosure,
  Button,
  Text,
  Input,
  Box,
  Flex,
  IconButton,
  PseudoBox,
  PseudoBoxProps,
} from '@chakra-ui/core'
import { CustomButton } from '@/components/common/CustomButton'
import { CustomModal } from '@/components/common/CustomModal'
import { CustomModalOverlay } from '@/components/common/CustomModalOverlay'
import { AiOutlinePlusCircle, AiFillMinusCircle } from 'react-icons/ai'

import { Topic } from '@/utils/api/proposal'

interface CreateVoiceModalProps {
  topics: string[]
  allTopics: Topic[]
  setTopics: Dispatch<SetStateAction<string[]>>
}

const CreateVoiceModal: React.FC<CreateVoiceModalProps> = (props) => {
  const { isOpen, onOpen, onClose } = useDisclosure()
  const [searchTopic, setSerchTopic] = useState('')

  const { topics, allTopics, setTopics } = props
  const [tempTopics, setTempTopics] = useState(topics)
  useEffect(() => {
    setTempTopics(topics)
  }, [topics])

  const selectTopic = (newTopic) => {
    if (tempTopics.length < 3) {
      setSerchTopic('')
      setTempTopics([...tempTopics, newTopic])
    }
  }

  const removeTempTopic = (removingTopic) => {
    setTempTopics(tempTopics.filter((topic) => topic !== removingTopic))
  }

  const removeTopic = (removingTopic) => {
    setTopics(topics.filter((topic) => topic !== removingTopic))
  }

  const handleSubmit = () => {
    setTopics(tempTopics)
    onClose()
  }

  const handleCancel = () => {
    setTempTopics(topics)
    onClose()
  }

  const handleChangeSearchText = ({ target }) => {
    setSerchTopic(target.value)
  }

  const currentTopics = allTopics.map((topic) => topic.name)
  const [newTopics, setNewTopics] = useState<string[]>([])
  const addNewTopic = () => {
    setNewTopics([...newTopics, searchTopic])
    selectTopic(searchTopic)
  }

  const filteredTopics = newTopics.concat(currentTopics).filter((topic) => {
    if (tempTopics.includes(topic)) {
      return false
    }
    if (searchTopic.length === 0) {
      return true
    }
    return topic.includes(searchTopic)
  })

  const SelectedTopicList = topics.map((topic) => (
    <Flex
      key={topic}
      direction="row"
      align="center"
      mb="8px"
      bg="chula.SecondaryPink"
      rounded="15px"
      minH="35px"
    >
      <IconButton
        isRound
        onClick={() => removeTopic(topic)}
        variantColor="PrimaryPink"
        variant="ghost"
        fontSize="24px"
        aria-label=""
        icon={AiFillMinusCircle}
      />
      <Text fontFamily="Sarabun" fontSize="16px" color="chula.Gray">
        {topic}
      </Text>
    </Flex>
  ))

  const TopicList = tempTopics.map((topic) => (
    <Flex
      key={topic}
      minH="40px"
      borderBottom={topic !== tempTopics[2] ? '1px' : '0px'}
      borderBottomColor="chula.SecondaryPink"
      justify="space-between"
      align="center"
    >
      <Text fontFamily="Sarabun" fontSize="16px" color="chula.Gray">
        {topic}
      </Text>
      <IconButton
        isRound
        onClick={() => removeTempTopic(topic)}
        variantColor="PrimaryPink"
        variant="ghost"
        fontSize="22px"
        aria-label=""
        icon={AiFillMinusCircle}
      />
    </Flex>
  ))

  const isTopicExist =
    searchTopic.trim().length === 0 ||
    [...currentTopics, ...newTopics].includes(searchTopic)

  return (
    <>
      <Flex direction="column">
        {SelectedTopicList}
        <Button
          h="35px"
          rounded="15px"
          variantColor="PrimaryPink"
          fontFamily="ChulaCharasNew"
          fontWeight="normal"
          fontSize="18px"
          leftIcon={AiOutlinePlusCircle}
          onClick={onOpen}
        >
          เพิ่มหัวข้อ
        </Button>
      </Flex>

      <CustomModal isOpen={isOpen} onClose={onClose}>
        <CustomModalOverlay />
        <ModalContent
          minH="675px"
          w="90%"
          border="1px"
          borderColor="chula.PrimaryPink"
          rounded="5px"
          p="16px"
        >
          <Input
            h="28px"
            mb="8px"
            fontFamily="Sarabun"
            fontSize="16px"
            placeholder="ค้นหาหัวข้อ"
            border="1px"
            borderColor="chula.PrimaryPink"
            focusBorderColor="chula.PrimaryPink"
            type="text"
            maxLength={25}
            onChange={handleChangeSearchText}
          />

          <Box
            borderWidth="1px"
            border="1px"
            borderColor="chula.PrimaryPink"
            rounded="5px"
          >
            <Flex
              overflow="auto"
              direction="column"
              px="16px"
              mb="6px"
              h="349px"
              min-height="auto"
            >
              {filteredTopics.map((topic) => (
                <TopicItem key={topic} onClick={() => selectTopic(topic)}>
                  {topic}
                </TopicItem>
              ))}
              {!isTopicExist && (
                <TopicItem onClick={addNewTopic}>
                  เพิ่มแท็ก: "{searchTopic}"
                </TopicItem>
              )}
            </Flex>

            <Box p="16px">
              <Text
                mb="8px"
                fontSize="24px"
                color="chula.PrimaryPink"
                fontFamily="CHULALONGKORN"
                fontWeight="bold"
                borderBottom="1px"
                borderBottomColor="chula.Gold"
              >
                หัวข้อที่เลือก ({tempTopics.length}/3)
              </Text>

              <Box
                px="9px"
                mb="9px"
                minH="128px"
                borderWidth="1px"
                borderColor="chula.PrimaryPink"
                rounded="5px"
              >
                {TopicList}
              </Box>

              <Flex direction="row" justify="space-between">
                <CustomButton
                  onClick={handleCancel}
                  w="133px"
                  h="45px"
                  variant="outline"
                  variantColor="PrimaryPink"
                  mr="8px"
                >
                  ยกเลิก
                </CustomButton>
                <CustomButton
                  w="133px"
                  h="45px"
                  variantColor="PrimaryPink"
                  onClick={handleSubmit}
                >
                  ยืนยัน
                </CustomButton>
              </Flex>
            </Box>
          </Box>
        </ModalContent>
      </CustomModal>
    </>
  )
}

export { CreateVoiceModal }

const TopicItem: React.FC<PseudoBoxProps> = ({ children, ...props }) => (
  <PseudoBox
    color="black"
    _hover={{
      color: 'chula.PrimaryPink',
    }}
    py="9px"
    fontFamily="Sarabun"
    fontSize="16px"
    transition="all 0.2s cubic-bezier(.08,.52,.52,1)"
    cursor="pointer"
    {...props}
  >
    {children}
    <Box height="1px" width="100%" bg="chula.SecondaryPink" />
  </PseudoBox>
)
