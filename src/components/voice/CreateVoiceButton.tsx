import { IconButton, Flex } from '@chakra-ui/core'
import { FiEdit } from 'react-icons/fi'
import Link from 'next/link'
import styled from '@emotion/styled'
import { useProposalContext } from '@/providers/ProposalProvider'

const CreateVoiceButtonStickyBox = styled(Flex)`
  position: fixed;
  position: sticky;
  bottom: 16px;
  right: 16px;
  padding-top: 16px;
`

const CreateVoiceButton: React.FC = () => {
  const { proposalId } = useProposalContext()
  return (
    <CreateVoiceButtonStickyBox
      flexDirection="row-reverse"
      pointerEvents="none"
    >
      <Link href={`/${proposalId}/createVoice`} passHref>
        <IconButton
          as="a"
          isRound
          size="lg"
          zIndex={1}
          variantColor="PrimaryPink"
          aria-label="Create voice"
          icon={FiEdit}
          pointerEvents="auto"
        />
      </Link>
    </CreateVoiceButtonStickyBox>
  )
}

export { CreateVoiceButton }
