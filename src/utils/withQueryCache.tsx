import { QueryCache, useQuery } from 'react-query'
import { dehydrate } from 'react-query/hydration'
import { NextPage, NextPageContext } from 'next'
import { useRouter } from 'next/router'
import { ParsedUrlQuery } from 'querystring'

const globalQueryCache = new QueryCache()

export declare type QueryCachePage<P> = React.ComponentType<P> & {
  getInitialProps?(query: ParsedUrlQuery): Promise<P>
}

export function getQueryCache(): QueryCache {
  if (typeof window !== 'undefined') {
    return globalQueryCache
  } else {
    return new QueryCache()
  }
}

export function withQueryCache<P>(Page: QueryCachePage<P>): NextPage<{}> {
  function withQueryCache() {
    const router = useRouter()
    const { data: props } = useQuery(['initialProps', router.asPath], () =>
      Page.getInitialProps(router.query)
    )
    return <Page {...props} />
  }

  withQueryCache.getInitialProps = async (
    context: NextPageContext
  ): Promise<{}> => {
    const queryCache = getQueryCache()
    const initialPropsKey = ['initialProps', context.asPath]
    const initialPropsPromise = queryCache.fetchQuery(initialPropsKey, () =>
      Page.getInitialProps(context.query)
    )
    if (queryCache.getQueryData(initialPropsKey) === undefined) {
      await initialPropsPromise
    }
    return { dehydratedState: dehydrate(queryCache) }
  }

  return withQueryCache
}

export { globalQueryCache as queryCache }
