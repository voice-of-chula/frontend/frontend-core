export let resolveAuthPromise: () => void
let authPromise = new Promise<void>((resolve) => {
  resolveAuthPromise = () => {
    if (typeof window === 'undefined') {
      throw new Error('this must not be called on the server side')
    }
    resolveAuthPromise = () => {}
    authPromise = null
    resolve()
  }
})

export async function waitForAuth(): Promise<void> {
  if (authPromise !== null) {
    return await authPromise
  }
}
