export const DEV = process.env.NODE_ENV === 'development'
export const APP_HOST = process.env.NEXT_PUBLIC_APP_HOST
export const REAL_API_HOST = process.env.NEXT_PUBLIC_API_HOST
export const API_HOST = DEV ? '/api' : REAL_API_HOST
export const SSO_URL = process.env.NEXT_PUBLIC_SSO_URL
export const GA = process.env.GA || 'UA-180265625-2'
