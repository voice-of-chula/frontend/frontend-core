export function exists(value: any): boolean {
  return typeof value !== 'undefined'
}
