import ErrorPage from '@/pages/_error'
import { NextPage, NextPageContext } from 'next'
import { ServerResponse } from 'http'

export interface ErrorPageProps<P> {
  pageProps?: P
  errorCode?: number
}

export function withErrorPage<P>(
  Page: NextPage<P>
): React.ComponentType<ErrorPageProps<P>> {
  function withErrorPage(props: ErrorPageProps<P>) {
    if (props.errorCode) {
      return <ErrorPage statusCode={props.errorCode} />
    }

    return <Page {...props.pageProps} />
  }

  if (Page.getInitialProps) {
    withErrorPage.getInitialProps = async (
      context: NextPageContext
    ): Promise<ErrorPageProps<P>> => {
      try {
        return { pageProps: await Page.getInitialProps(context) }
      } catch (err) {
        return checkHttpError(context.res, err).props
      }
    }
  }

  return withErrorPage
}

export function checkHttpError(res: ServerResponse, err: any) {
  if (err.response?.status) {
    const status = err.response.status
    res.statusCode = status
    return {
      props: {
        errorCode: status,
      },
    }
  } else {
    throw err
  }
}
