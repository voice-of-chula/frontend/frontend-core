import { ParsedUrlQuery } from 'querystring'
import { useRouter } from 'next/router'
import React, { PropsWithChildren, useCallback } from 'react'
import { PageSpinner } from '@/components/layout/PageSpinner'
import { NextPageContext, NextPage } from 'next'
import { mightHaveToken } from '.'
import ErrorPage from '@/pages/_error'
import { globalAuthState, useAuthContext } from '@/providers/AuthProvider'
import { getQueryCache } from './withQueryCache'
import { useQuery } from 'react-query'
import { dehydrate } from 'react-query/hydration'
import { waitForAuth } from './waitForAuth'

export declare type AuthedFetchPage<P, A> = React.ComponentType<P & A> & {
  getInitialProps?(query: ParsedUrlQuery): Promise<P>
  fetchAuthedData(query: ParsedUrlQuery): Promise<A>
  ContentWrapper?: React.ComponentType<PropsWithChildren<{}>>
}

export type WithAuthedFetchProps<P, C> = P & {
  authedData: C
}

export function withAuthedFetch<IP, C>(
  Page: AuthedFetchPage<IP, C>
): NextPage<{}> {
  const getInitialProps = Page.getInitialProps
    ? Page.getInitialProps
    : () => ({})
  const { ContentWrapper } = Page

  function withAuthedFetch() {
    const router = useRouter()
    const { ouid, isPending } = useAuthContext()
    const initialPropsFetcher = useCallback(
      () => getInitialProps(router.query),
      [router.query]
    )
    const authedDataFetcher = useCallback(
      () => Page.fetchAuthedData(router.query),
      [router.query]
    )
    const { data: initialProps } = useQuery(
      ['initialProps', router.asPath],
      initialPropsFetcher
    )
    const { isError, error, data: authedData } = useQuery<C, any>(
      ['authedData', router.asPath, ouid],
      authedDataFetcher,
      { enabled: !isPending }
    )

    let content: JSX.Element
    if (isError) {
      content = <ErrorPage statusCode={error?.response?.status} />
    } else if (!authedData) {
      content = <PageSpinner />
    } else {
      content = <Page {...(initialProps as IP)} {...authedData} />
    }

    if (ContentWrapper) {
      return <ContentWrapper>{content}</ContentWrapper>
    } else {
      return content
    }
  }

  withAuthedFetch.getInitialProps = async (
    context: NextPageContext
  ): Promise<{}> => {
    const queryCache = getQueryCache()
    const initialPropsKey = ['initialProps', context.asPath]
    const promises = []
    const initialPropsPromise = queryCache.fetchQuery(initialPropsKey, () =>
      getInitialProps(context.query)
    )
    if (queryCache.getQueryData(initialPropsKey) === undefined) {
      promises.push(initialPropsPromise)
    }

    if (typeof window !== 'undefined' || !mightHaveToken(context.req)) {
      if (typeof window !== 'undefined') {
        await waitForAuth()
      }
      const authedDataKey = ['authedData', context.asPath, globalAuthState.ouid]
      const authedDataPromise = queryCache.fetchQuery(authedDataKey, () =>
        Page.fetchAuthedData(context.query)
      )
      if (queryCache.getQueryData(authedDataKey) === undefined) {
        promises.push(authedDataPromise)
      }
    }

    await Promise.all(promises)
    return { dehydratedState: dehydrate(queryCache) }
  }

  return withAuthedFetch
}
