import { format, parseISO, formatDistance } from 'date-fns'
import { IncomingMessage } from 'http'
import { NextRouter } from 'next/router'
import { useCallback, useEffect, useMemo, useState } from 'react'
import { APP_HOST, SSO_URL } from './config'
import { QueryKey, useInfiniteQuery } from 'react-query'
import { useToast, useToastOptions } from '@chakra-ui/core'

function useCanShare() {
  const [canShare, setCanShare] = useState(false)

  useEffect(() => {
    setCanShare(!!navigator.share)
  }, [])

  return canShare
}

export function useShare(title: string, url: string) {
  const canShare = useCanShare()

  const shareData = useMemo(
    () => ({
      title,
      url,
    }),
    [title, url]
  )

  const share = useCallback(() => {
    navigator.share(shareData).catch(() => {})
  }, [shareData])

  return { canShare, share }
}

export type LoadAfter<T> = (count: number, lastItem?: T) => Promise<T[]>

export interface LoadMoreResult<T> {
  data: T[]
  hasMore: boolean
  loadMore: () => Promise<void>
  loading: boolean
}

export function useLoadMore<T>(
  queryKey: QueryKey,
  loadAfter: LoadAfter<T>,
  count: number,
  initialData: T[] = null,
  initialHasMore: boolean = true
): LoadMoreResult<T> {
  const fetchData = useCallback(
    async (_key, cursor) => {
      const data = await loadAfter(count + 1, cursor)
      let lastCursor = null
      if (data.length > count) {
        data.pop()
        lastCursor = data[data.length - 1]
      }
      return { data, lastCursor }
    },
    [loadAfter]
  )
  const {
    status,
    data = [],
    isFetching,
    isFetchingMore,
    fetchMore,
    canFetchMore,
  } = useInfiniteQuery([queryKey], fetchData, {
    getFetchMore: (lastGroup) => lastGroup.lastCursor,
    initialData:
      initialData !== null
        ? [
            {
              data: initialData,
              lastCursor: initialHasMore
                ? initialData[initialData.length - 1]
                : null,
            },
          ]
        : undefined,
  })

  const flattenedData = useMemo(
    () =>
      data.reduce((previous, current) => [...previous, ...current.data], []),
    [data]
  )

  const loadMore = useCallback(async () => {
    await fetchMore()
  }, [fetchMore])

  return {
    data: flattenedData,
    hasMore: canFetchMore,
    loadMore,
    loading: isFetchingMore !== false,
  }
}

export function mightHaveToken(req: IncomingMessage): boolean {
  if (process.browser) {
    return true
  }
  const cookie = req?.headers?.cookie || ''
  return cookie.indexOf('voice_of_chula_refresh_token') !== -1
}

export function getLoginURL(router: NextRouter): string {
  return `${SSO_URL}/login?service=${APP_HOST}/callback?r=${router.asPath}`
}

export function getLoginURLForPath(path: string): string {
  return `${SSO_URL}/login?service=${APP_HOST}/callback?r=${path}`
}

export function useReverseArray<T>(array: T[]): T[] {
  return useMemo(() => {
    const tmp = [...array]
    tmp.reverse()
    return tmp
  }, [array])
}

export function useParseOuid(ouid: string) {
  return useMemo(
    () => ({
      year: parseInt(ouid.slice(0, 2)),
      facultyId: parseInt(ouid.slice(-2)),
    }),
    []
  )
}

export function useOwnerInfo(ouid: string): string {
  const { year, facultyId } = useParseOuid(ouid)
  const thisYear = parseInt(`${new Date().getFullYear() + 543}`.slice(2))
  const academicYear = new Date().getMonth() < 7 ? thisYear - 1 : thisYear

  return useMemo(() => {
    const faculty = facultyNames[facultyId]
    return (
      (faculty ? faculty + ' ' : '') + `ชั้นปีที่ ${academicYear - year + 1}`
    )
  }, [year, facultyId])
}

export const facultyNames = {
  21: 'คณะวิศวกรรมศาสตร์',
  22: 'คณะอักษรศาสตร์',
  23: 'คณะวิทยาศาสตร์',
  24: 'คณะรัฐศาสตร์',
  25: 'คณะสถาปัตยกรรมศาสตร์',
  26: 'คณะพาณิชยศาสตร์และการบัญชี',
  27: 'คณะครุศาสตร์',
  28: 'คณะนิเทศศาสตร์',
  29: 'คณะเศรษฐศาสตร์',
  30: 'คณะแพทยศาสตร์',
  31: 'คณะสัตวแพทยศาสตร์',
  32: 'คณะทันตแพทยศาสตร์',
  33: 'คณะเภสัชศาสตร์',
  34: 'คณะนิติศาสตร์',
  35: 'คณะศิลปกรรมศาสตร์',
  36: 'คณะพยาบาลศาสตร์',
  37: 'คณะสหเวชศาสตร์',
  38: 'คณะจิตวิทยา',
  39: 'คณะวิทยาศาสตร์การกีฬา',
  40: 'สำนักวิชาทรัพยากรการเกษตร',
  56: 'สถาบันนวัตกรรมบูรณาการ',
}

export function formatPlural(count: number, word: string) {
  if (count === 1) {
    return word
  } else {
    return word + 's'
  }
}

export function useRelativeTime(iso: string): string {
  return useMemo(() => {
    const date = parseISO(iso)
    return formatDistance(date, new Date())
  }, [iso])
}

export function useFormatTime(iso: string): string {
  return useMemo(() => {
    const date = parseISO(iso)
    const year = (date.getFullYear() + 543) % 100
    return format(date, 'HH:mm น. - d/M/' + year)
  }, [iso])
}

export function useDeviceDetect() {
  const [isMobile, setMobile] = useState(false)

  useEffect(() => {
    const userAgent = window?.navigator.userAgent ?? ''
    const mobile = Boolean(
      userAgent.match(
        /Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile|WPDesktop/i
      )
    )
    const isIOS =
      (/iPad|iPhone|iPod/.test(navigator.platform) ||
        (navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1)) &&
      !window.MSStream
    setMobile(mobile || isIOS)
  }, [])

  return { isMobile }
}

export function useCustomToast() {
  const toast = useToast()
  const successToast = useCallback(
    (options: useToastOptions) => {
      toast({
        status: 'success',
        duration: 1500,
        isClosable: true,
        ...options,
      })
    },
    [toast]
  )
  const errorToast = useCallback(
    (options: useToastOptions) => {
      toast({
        status: 'error',
        duration: 1500,
        isClosable: true,
        ...options,
      })
    },
    [toast]
  )
  return { successToast, errorToast }
}

export function useToastFeedback<T extends readonly unknown[], R>(
  action: string,
  func: (...args: T) => Promise<R>
): (...args: T) => Promise<R> {
  const { successToast, errorToast } = useCustomToast()
  return useCallback(
    async (...args) => {
      try {
        const result = await func(...args)
        successToast({ title: `${action}แล้ว` })
        return result
      } catch (e) {
        errorToast({ title: `เกิดข้อผิดพลาดในการ${action}` })
        throw e
      }
    },
    [func, successToast, errorToast]
  )
}

export function cachePromise<T extends readonly unknown[], R>(
  func: (...args: T) => Promise<R>
) {
  let promise: Promise<R> | null = null
  return (...args: T) => {
    if (promise !== null) {
      return promise
    }
    promise = func(...args)
    const clear = () => (promise = null)
    promise.then(clear, clear)
    return promise
  }
}
