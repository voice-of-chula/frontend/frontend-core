import {
  VoiceDto,
  upvoteVoice,
  ReplyDto,
  replyVoice,
  getMoreReplies,
  reportVoice,
  downvoteVoice,
} from './api/voice'
import { useState, useCallback, useMemo, useEffect } from 'react'
import { useProposalContext } from '@/providers/ProposalProvider'
import { useRequireLogin } from '@/components/common/RequireLoginModal'
import { APP_HOST } from './config'
import { useAuthContext } from '@/providers/AuthProvider'
import {
  useLoadMore,
  LoadAfter,
  useReverseArray,
  getLoginURLForPath,
  useToastFeedback,
} from '.'
import { useQueryCache } from 'react-query'
import { voicePagePath } from '@/pages/[proposalId]/voice/[voiceId]'

function useAssign<T>(target: T, source: Partial<T>): T {
  return useMemo(() => {
    const tmp = { ...target }
    Object.assign(tmp, source)
    return tmp
  }, [target, source])
}

let currentPendingId = 0

export function useReplies(voice: VoiceDto) {
  const { proposalId } = useProposalContext()
  const { _id: voiceId } = voice
  const { ouid } = useAuthContext()

  const loadAfter: LoadAfter<ReplyDto> = useCallback(
    async (count, reply) => {
      const replies = await getMoreReplies(
        proposalId,
        voiceId,
        count,
        reply?._id || 'ffffffffffffffffffffffff'
      )
      replies.reverse()
      return replies
    },
    [proposalId, voiceId]
  )

  const repliesKey = useMemo(
    () => ({ ouid, proposalId, type: 'replies', voiceId }),
    [ouid, proposalId, voiceId]
  )

  const reversedRepliesDto = useReverseArray(voice.replies)
  const loadMoreReplies = useLoadMore(
    repliesKey,
    loadAfter,
    5,
    reversedRepliesDto,
    voice.replies.length < voice.replyCount
  )
  const { data: reversedReplies } = loadMoreReplies
  const replies = useReverseArray(reversedReplies)
  const [submittedCount, setSubmittedCount] = useState(0)
  const [newReplies, setNewReplies] = useState<ReplyDto[]>([])
  useEffect(() => {
    setNewReplies((newReplies) => {
      const filtered = newReplies.filter(
        (reply) => reply.pendingId || reply.error
      )
      const removed = newReplies.length - filtered.length
      setSubmittedCount((removedCount) => removedCount + removed)
      return filtered
    })
  }, [replies])
  const clientReplies = useMemo(() => [...replies, ...newReplies], [
    replies,
    newReplies,
  ])
  const newRepliesCount = useMemo(() => {
    let count = 0
    newReplies.forEach((reply) => {
      if (!reply.pendingId && !reply.error) {
        count++
      }
    })
    return count + submittedCount
  }, [newReplies, submittedCount])
  const replyCount = voice.replyCount + newRepliesCount

  const markAsDone = useCallback((pendingId: number) => {
    setNewReplies((newReplies) =>
      newReplies.map((reply) => {
        if (reply.pendingId !== pendingId) {
          return reply
        } else {
          const { pendingId, ...rest } = reply
          return rest
        }
      })
    )
  }, [])

  const markAsError = useCallback((pendingId: number) => {
    setNewReplies((newReplies) =>
      newReplies.map((reply) => {
        if (reply.pendingId !== pendingId) {
          return reply
        } else {
          const { pendingId, ...rest } = reply
          return { ...rest, error: true }
        }
      })
    )
  }, [])

  const queryCache = useQueryCache()
  const submitReply = useCallback(
    async (detail: string) => {
      const pendingId = ++currentPendingId
      const reply: ReplyDto = {
        _id: `client-reply-${pendingId}`,
        owner: ouid,
        detail,
        pendingId,
      }
      setNewReplies((newReplies) => [...newReplies, reply])
      try {
        await replyVoice(proposalId, voiceId, { detail })
        markAsDone(pendingId)
        queryCache.invalidateQueries(repliesKey)
      } catch (err) {
        markAsError(pendingId)
      }
    },
    [proposalId, voiceId, markAsDone, markAsError, queryCache, repliesKey]
  )

  return { loadMoreReplies, replies: clientReplies, replyCount, submitReply }
}

type Vote = 'up' | 'down' | 'neutral'

export function useVoice(voiceDto: VoiceDto) {
  const { proposalId } = useProposalContext()
  const [voice, setVoice] = useState(voiceDto)
  const [voteOverlay, setVoteOverlay] = useState<Partial<VoiceDto>>({})
  const clientVoice = useAssign(voice, voteOverlay)
  const { selfUpvoted, selfDownvoted } = clientVoice
  const voicePath = voicePagePath(proposalId, voice._id)
  const voiceURL = `${APP_HOST}${voicePath}`

  useEffect(() => setVoice(voiceDto), [voiceDto])

  const setVoteInternal = useCallback(
    async (vote: Vote) => {
      const upvotesExcludingMyself = voice.upvotes - (voice.selfUpvoted ? 1 : 0)
      const downvotesExcludingMyself =
        voice.downvotes - (voice.selfDownvoted ? 1 : 0)
      const upvote = vote === 'up'
      const downvote = vote === 'down'
      setVoteOverlay({
        selfUpvoted: upvote,
        selfDownvoted: downvote,
        upvotes: upvotesExcludingMyself + (upvote ? 1 : 0),
        downvotes: downvotesExcludingMyself + (downvote ? 1 : 0),
      })
      try {
        if (vote === 'down') {
          await downvoteVoice(proposalId, voiceDto._id, downvote)
        } else {
          await upvoteVoice(proposalId, voiceDto._id, upvote)
        }
      } catch (err) {
        setVoteOverlay({})
      }
    },
    [proposalId, selfUpvoted, voice]
  )

  const onReportInternal = useCallback(async () => {
    await reportVoice(proposalId, voiceDto._id)
  }, [])
  const onReport = useToastFeedback('รายงานเสียง', onReportInternal)

  const loginURL = getLoginURLForPath(voicePath)

  const { func: setVote, modal } = useRequireLogin(
    'โหวตเสียงนี้',
    setVoteInternal,
    loginURL
  )

  const toggleUpvote = useCallback(() => {
    if (selfUpvoted) {
      setVote('neutral')
    } else {
      setVote('up')
    }
  }, [selfUpvoted])

  const toggleDownvote = useCallback(() => {
    if (selfDownvoted) {
      setVote('neutral')
    } else {
      setVote('down')
    }
  }, [selfDownvoted])

  const modals = <>{modal}</>

  return {
    voice: clientVoice,
    toggleUpvote,
    toggleDownvote,
    onReport,
    modals,
    voiceURL,
    loginURL,
    ...useReplies(voice),
  }
}
