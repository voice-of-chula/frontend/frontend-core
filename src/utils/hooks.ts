import { useRouter } from 'next/router'

export function useNewRedirect() {
  const { asPath, query } = useRouter()

  const redirectParam = query.r ? `?r=${query.r}` : ''
  return `?r=${asPath}${redirectParam}`
}
