import { EventEmitter } from 'events'
import { refreshToken } from './api/auth'
import { cachePromise } from '.'
import jwt_decode from 'jwt-decode'

type TokenChangeListener = (accessToken: AccessToken) => void

export class TokenManager {
  accessToken: AccessToken | null = null
  refreshPromise: Promise<AccessToken | null> | null = null
  tokenChangeListener: TokenChangeListener

  constructor(tryRefresh: boolean, tokenChangeListener: TokenChangeListener) {
    this.tokenChangeListener = tokenChangeListener
    if (tryRefresh) {
      if (typeof window === 'undefined') return
      this.refresh()
    } else {
      this.setToken(null)
    }
  }

  refresh = cachePromise(async () => {
    try {
      const { accessToken } = await refreshToken()
      return this.setToken(accessToken)
    } catch (err) {
      if (err.isAxiosError) {
        return this.setToken(null)
      }
    }
  })

  setToken(token: string | null) {
    if (token === null) {
      this.accessToken = null
    } else {
      this.accessToken = new AccessToken(token)
    }
    this.tokenChangeListener(this.accessToken)
    return this.accessToken
  }

  getAccessToken = async () => {
    if (this.accessToken === null) {
      return null
    }
    if (this.accessToken.isExpired()) {
      await this.refresh()
    }
    return this.accessToken
  }
}

interface TokenData {
  ouid: string | null
  exp: number
}

export class AccessToken {
  accessToken: string
  tokenData: TokenData

  constructor(accessToken: string) {
    this.accessToken = accessToken
    this.tokenData = jwt_decode(accessToken)
  }

  isExpired() {
    return Date.now() >= this.tokenData.exp * 1000
  }
}
