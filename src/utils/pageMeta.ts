import { EventEmitter } from 'events'
import { useEffect, useState } from 'react'
import withSideEffect from 'react-side-effect'

interface PageMetaProps {
  pageTitle?: string
  title?: string
  titleHref?: string
  meta?: { [key: string]: string }
  navBarItems?: React.ReactNode
  searchButton?: React.ReactNode
}

interface PageMetaState {
  appTitle: string
  pageTitle: string
  title: string
  titleHref?: string
  meta: { [key: string]: any }
  navBarItems?: React.ReactNode
  searchButton?: React.ReactNode
}

const PageMeta: React.FC<PageMetaProps> = () => {
  return null
}

const defaultState: PageMetaState = {
  appTitle: 'Voice of Chula',
  pageTitle: '',
  title: '',
  meta: {
    title: 'Voice of Chula',
    name: 'Voice of Chula',
    description:
      'ระบบรวบรวมเสียงนิสิตจุฬาลงกรณ์มหาวิทยาลัย เพื่อประกอบการนำเสนอนโยบายเพื่อส่วนรวม และส่งเสริมวัฒนธรรมการมีส่วนร่วมทางการเมืองในระบอบประชาธิปไตย',
    'og:type': 'website',
    'og:locale': 'th_TH',
    'og:locale:alternate': 'en_US',
    'og:image:width': 1200,
    'og:image:height': 630,
    'twitter:card': 'summary_large_image',
  },
}

function reducePropsToState(propsList: PageMetaProps[]): PageMetaState {
  const state: PageMetaState = { ...defaultState }
  propsList.forEach((props) => {
    const { meta = {}, ...otherProps } = props
    // assign meta
    state.meta = { ...defaultState.meta }
    Object.assign(state.meta, meta)
    // assign title to pageTitle
    if (props.title) {
      state.pageTitle = props.title
    }
    // assign the rest
    Object.assign(state, otherProps)
  })
  return state
}

let currentState = null
const emitter = new EventEmitter()

function handleStateChangeOnClient(state: PageMetaState) {
  currentState = state
  emitter.emit('change', state)
}

const PageMetaSideEffects = withSideEffect(
  reducePropsToState,
  handleStateChangeOnClient
)(PageMeta)

function usePageMeta(): PageMetaState {
  const [state, setState] = useState(currentState)

  useEffect(() => {
    const listener = (newState: PageMetaState) => {
      setState(newState)
    }
    emitter.addListener('change', listener)
    return () => emitter.removeListener('change', listener)
  })

  return state || PageMetaSideEffects.peek()
}

export { PageMetaSideEffects as PageMeta, usePageMeta }
