import { get, post, del, put } from './http'

interface voiceCountDto {
  count: number
}

export interface ReplyDto {
  _id: string
  owner: string
  detail: string
  pendingId?: number
  error?: boolean
}

export interface VoiceDto {
  _id: string
  owner: string
  detail: string
  reports?: string[]
  upvotes?: number
  downvotes?: number
  selfUpvoted: boolean
  selfDownvoted: boolean
  selfReported: boolean
  topics: string[]
  replies?: ReplyDto[]
  replyCount: number
  createdTime: string
}

export interface CreateVoiceDto extends Partial<VoiceDto> {
  detail: string
  topics: string[]
}

export interface ReplyVoiceDto {
  detail: string
}

export interface UpvoteDto {
  upvote: boolean
}

export interface DownvoteDto {
  downvote: boolean
}

export async function getVoiceCount(proposalId: string): Promise<number> {
  const result = await get<voiceCountDto>(`/proposal/${proposalId}/voice/count`)
  return result.count
}

export interface GetVoiceParams {
  limit?: number
  afterVoiceId?: string
}

export const DEFAULT_VOICE_COUNT = 10

export async function getVoices(
  proposalId: string,
  params?: GetVoiceParams
): Promise<VoiceDto[]> {
  return await get(`/proposal/${proposalId}/voice`, params)
}

export async function getVoicesFromTopic(
  proposalId: string,
  topic: string,
  params?: GetVoiceParams
): Promise<VoiceDto[]> {
  return await get(`/proposal/${proposalId}/topicQuery`, {
    topic: topic,
    ...params,
  })
}

export async function getMyVoices(
  proposalId: string,
  params?: GetVoiceParams
): Promise<VoiceDto[]> {
  return await get(`/proposal/${proposalId}/voice/me`, params)
}

export async function createVoice(
  proposalId: string,
  createVoiceDto: CreateVoiceDto
): Promise<VoiceDto> {
  return await post(`/proposal/${proposalId}/voice`, createVoiceDto)
}

export async function deleteVoice(proposalId: string, voiceId: string) {
  await del(`/proposal/${proposalId}/voice/${voiceId}`)
}

export async function reportVoice(proposalId: string, voiceId: string) {
  await put(`/proposal/${proposalId}/voice/${voiceId}/report`)
}

export async function upvoteVoice(
  proposalId: string,
  voiceId: string,
  upvote: boolean
): Promise<VoiceDto> {
  return await put(`/proposal/${proposalId}/voice/${voiceId}/upvote`, {
    upvote,
  })
}

export async function downvoteVoice(
  proposalId: string,
  voiceId: string,
  downvote: boolean
): Promise<VoiceDto> {
  return await put(`/proposal/${proposalId}/voice/${voiceId}/downvote`, {
    downvote,
  })
}

export async function replyVoice(
  proposalId: string,
  voiceId: string,
  replyVoiceDto: ReplyVoiceDto
): Promise<VoiceDto> {
  return await post(
    `/proposal/${proposalId}/voice/${voiceId}/reply`,
    replyVoiceDto
  )
}

export async function getMoreReplies(
  proposalId: string,
  voiceId: string,
  limit: number,
  beforeReplyId: string
): Promise<ReplyDto[]> {
  return await get(`/proposal/${proposalId}/voice/${voiceId}/reply`, {
    limit,
    before: beforeReplyId,
  })
}

export async function getVoice(
  proposalId: string,
  voiceId: string
): Promise<VoiceDto> {
  return await get(`/proposal/${proposalId}/voice/${voiceId}`)
}
