import { post } from './http'

export interface ExchangeTicketDto {
  ticket: string
}

export interface WebappTokensDto {
  accessToken: string
}

export async function exchangeTicket(ticket: string): Promise<WebappTokensDto> {
  return await post<ExchangeTicketDto, WebappTokensDto>(
    `/auth/exchangeticket`,
    { ticket }
  )
}

export async function refreshToken(): Promise<WebappTokensDto> {
  return await post<{}, WebappTokensDto>(`/auth/refresh_token`)
}

export async function logout() {
  await post(`/auth/logout`)
}
