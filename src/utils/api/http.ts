import axios from 'axios'
import { API_HOST, REAL_API_HOST } from '../config'

const httpClient = axios.create({
  baseURL: typeof window === 'undefined' ? REAL_API_HOST : API_HOST,
  withCredentials: true,
})

async function get<T>(url: string, params?: any): Promise<T> {
  return (await httpClient.get(url, { params })).data
}

async function post<D, T>(url: string, data?: D): Promise<T> {
  return (await httpClient.post(url, data)).data
}

async function put<D, T>(url: string, data?: D): Promise<T> {
  return (await httpClient.put(url, data)).data
}

async function del(url: string): Promise<void> {
  await httpClient.delete(url)
}

export { httpClient, get, post, put, del }
