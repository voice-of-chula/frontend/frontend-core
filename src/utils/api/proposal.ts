import { get } from './http'
import { VoiceDto } from './voice'

export interface Action {
  createdAt: string
  detail: string
}

export interface Proposal {
  _id: string
  name: string
  description: string
  actions?: Action[]
  popularVoices?: VoiceDto[]
}

export interface Topic {
  name: string
  count: number
}

export interface AllProposalDto {
  count: number
  proposals: Proposal[]
}

export function getProposals(): Promise<Proposal[]> {
  return get('/proposal')
}

export function getProposal(proposalId: string): Promise<Proposal> {
  return get(`/proposal/${proposalId}`)
}

export function getProposalsWithCount(): Promise<AllProposalDto> {
  return get(`/proposal/all`)
}

export async function getTopicsFromProposal(
  proposalId: string
): Promise<Topic[]> {
  return get(`/proposal/${proposalId}/topic`)
}
