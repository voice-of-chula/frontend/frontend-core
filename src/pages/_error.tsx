import { Button, Text } from '@chakra-ui/core'
import Link from 'next/link'
import { MdChevronLeft } from 'react-icons/md'
import { PageContainer } from '@/components/layout/PageContainer'
import { PageMeta } from '@/utils/pageMeta'

interface ErrorPageProps {
  statusCode?: number
  message?: string
}

function ErrorPage({ statusCode, message }: ErrorPageProps) {
  const displayMessage =
    message || statusCode === 404
      ? `ไม่พบหน้านี้`
      : `เกิดข้อผิดพลาด ${statusCode}`
  return (
    <PageContainer justifyContent="center" textAlign="center">
      <PageMeta pageTitle={displayMessage} />
      <Text
        fontFamily="CHULALONGKORN"
        fontSize="48px"
        color="chula.PrimaryPink"
      >
        {displayMessage}
      </Text>
      <div>
        <Link href="/" passHref>
          <Button
            as="a"
            variant="link"
            fontFamily="ChulaCharasNew"
            fontSize="18px"
            padding={2}
            variantColor="Gold"
            color="chula.Copper"
            leftIcon={MdChevronLeft}
          >
            กลับหน้าหลัก
          </Button>
        </Link>
      </div>
    </PageContainer>
  )
}

ErrorPage.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404
  return { statusCode }
}

export default ErrorPage
