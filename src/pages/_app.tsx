import { CSSReset, Flex, theme, ThemeProvider } from '@chakra-ui/core'
import { NextPage, NextPageContext } from 'next'
import Head from 'next/head'
import Router from 'next/router'
import {
  ReactQueryCacheProvider,
  ReactQueryConfig,
  ReactQueryConfigProvider,
} from 'react-query'
import { ReactQueryDevtools } from 'react-query-devtools'
import { Hydrate } from 'react-query/hydration'
import { Footer } from '@/components/layout/Footer'
import { LoadingIndicator } from '@/components/common/LoadingIndicator'
import Metas from '@/components/common/Metas'
import NavBar from '@/components/layout/NavBar'
import { SafeArea } from '@/components/common/SafeArea'
import AuthProvider from '@/providers/AuthProvider'
import '@/styles/globals.css'
import '@/styles/nprogress.css'
import { mightHaveToken } from '@/utils'
import { GA } from '@/utils/config'
import { PageMeta } from '@/utils/pageMeta'
import { queryCache } from '@/utils/withQueryCache'

const customTheme = {
  ...theme,

  colors: {
    ...theme.colors,
    brand: {
      500: '#F25278',
      700: '#D92A54',
    },
    chula: {
      PrimaryPink: '#DE5C8E',
      SecondaryPink: '#F8E1EA',
      TransparentPink: '#e57da5',
      Gray: '#58595B',
      Copper: '#AB5814',
      Gold: '#E9C869',
      White: '#FFFFFF',
    },
    PrimaryPink: {
      50: '#FCEEF3',
      100: '#F9DCE6',
      500: '#DE5C8E',
      600: '#D9457E',
      700: '#D53472',
    },
    Gold: {
      50: '#F7ECCA',
      100: '#F2DFA6',
      500: '#E9C869',
    },
    Gray: {
      50: '#D0D0D0',
      100: '#C4C4C4',
      200: '#979797',
    },
    Faculty: {
      1: '#DE5C8E', //Chulalongkorn (default)
      21: '#800000', //คณะวิศวกรรมศาสตร์
      22: '#999999', //คณะอักษรศาสตร์
      23: '#FFFF00', //คณะวิทยาศาสตร์
      24: '#1C1C1C', //คณะรัฐศาสตร์
      25: '#993300', //คณะสถาปัตยกรรมศาสตร์
      26: '#00CCFF', //คณะพาณิชยศาสตร์และการบัญชี
      27: '#FF3300', //คณะครุศาสตร์
      28: '#000080', //คณะนิเทศศาสตร์
      29: '#FFCC00', //คณะเศรษฐศาสตร์
      30: '#006600', //คณะแพทยศาสตร์
      31: '#66CCCC', //คณะสัตวแพทยศาสตร์
      32: '#330099', //คณะทันตแพทยศาสตร์
      33: '#66CC33', //คณะเภสัชศาสตร์
      34: '#EBEBEB', //คณะนิติศาสตร์
      35: '#CC0000', //คณะศิลปกรรมศาสตร์
      36: '#FF0000', //คณะพยาบาลศาสตร์
      37: '#CC99FF', //คณะสหเวชศาสตร์
      38: '#3300FF', //คณะจิตวิทยา
      39: '#FF6600', //คณะวิทยาศาสตร์การกีฬา
      40: '#7E2811', //สำนักวิชาทรัพยากรการเกษตร (SAR)
      56: '#DE5C8E', //สถาบันนวัตกรรมบูรณาการ (BAScii)(not found)
    },
  },
}

const reactQueryConfig: ReactQueryConfig = {
  queries: {
    staleTime: Infinity,
  },
}

function Providers({ pageProps, children }) {
  let realPageProps = pageProps?.pageProps || pageProps
  return (
    <ThemeProvider theme={customTheme}>
      <ReactQueryCacheProvider queryCache={queryCache}>
        <ReactQueryConfigProvider config={reactQueryConfig}>
          <Hydrate state={realPageProps?.dehydratedState}>{children}</Hydrate>
          {process.env.NODE_ENV === 'development' && (
            <style>{`.ReactQueryDevtools button { width: 40px; }`}</style>
          )}
          <ReactQueryDevtools />
        </ReactQueryConfigProvider>
      </ReactQueryCacheProvider>
    </ThemeProvider>
  )
}

function MyApp({ Component, pageProps, tryRefreshToken }) {
  return (
    <AuthProvider tryRefreshToken={tryRefreshToken}>
      <Providers pageProps={pageProps}>
        <CSSReset />
        <Head>
          <link rel="icon" href="/favicon.ico" />
          <link
            rel="preload"
            href="/fonts/ChulaCharasNewReg.ttf"
            as="font"
            type="font/ttf"
            crossOrigin="anonymous"
          />
          <link
            rel="preload"
            href="/fonts/SarabunReg.ttf"
            as="font"
            type="font/ttf"
            crossOrigin="anonymous"
          />
          <link
            rel="preload"
            href="/fonts/CHULALONGKORNBold.otf"
            as="font"
            type="font/otf"
            crossOrigin="anonymous"
          />
          <link
            rel="preload"
            href="/fonts/CHULALONGKORNReg.otf"
            as="font"
            type="font/otf"
            crossOrigin="anonymous"
          />
          <meta name="viewport" content="initial-scale=1, viewport-fit=cover" />
          <meta
            name="theme-color"
            content={customTheme.colors.chula.PrimaryPink}
          />
        </Head>
        <Flex direction="column" minH="100vh">
          <Flex direction="column-reverse" flex={1}>
            <PageMeta />
            <SafeArea flex={1} display="flex" flexDirection="column">
              <Component {...pageProps} />
            </SafeArea>
            {/* wait for the page to publish its meta before rendering the navbar */}
            <Metas />
            <NavBar />
          </Flex>
          <Footer />
        </Flex>
        <LoadingIndicator />
      </Providers>
    </AuthProvider>
  )
}

MyApp.getInitialProps = async ({
  Component,
  ctx,
}: {
  Component: NextPage
  ctx: NextPageContext
}) => {
  let pageProps = undefined
  if (Component.getInitialProps) {
    pageProps = await Component.getInitialProps(ctx)
  }
  return { pageProps, tryRefreshToken: mightHaveToken(ctx.req) }
}

Router.events.on('routeChangeComplete', () =>
  (window as any).gtag('config', GA, { page_path: window.location.pathname })
)

export default MyApp
