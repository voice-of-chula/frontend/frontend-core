import { Box, Stack, Text } from '@chakra-ui/core'
import Link from 'next/link'
import { ParsedUrlQuery } from 'querystring'
import { useCallback, useMemo } from 'react'
import CountUp from 'react-countup'
import { AiFillSound, AiOutlineShareAlt } from 'react-icons/ai'
import { ActionsCard } from '@/components/proposal/ActionsCard'
import { CreateVoiceButton } from '@/components/voice/CreateVoiceButton'
import { CustomButton } from '@/components/common/CustomButton'
import { PageContainer } from '@/components/layout/PageContainer'
import { PinkCard } from '@/components/common/PinkCard'
import { ReadMore } from '@/components/common/ReadMore'
import { SectionHead } from '@/components/common/SectionHead'
import { Share } from '@/components/common/Share'
import TopicItem from '@/components/topic/TopicItem'
import TopicSelectModal from '@/components/topic/TopicSelectModal'
import { VoiceCard } from '@/components/voice/VoiceCard'
import { VoiceList } from '@/components/voice/VoiceList'
import { useAuthContext } from '@/providers/AuthProvider'
import { RouterProposalProvider } from '@/providers/ProposalProvider'
import { LoadAfter } from '@/utils'
import {
  getProposal,
  getTopicsFromProposal,
  Proposal,
  Topic,
} from '@/utils/api/proposal'
import { getVoiceCount, getVoices, VoiceDto } from '@/utils/api/voice'
import { API_HOST, APP_HOST } from '@/utils/config'
import { PageMeta } from '@/utils/pageMeta'
import promiseAllProperties from '@/utils/promiseAllProperties'
import { AuthedFetchPage, withAuthedFetch } from '@/utils/withAuthedFetch'
import { withErrorPage } from '@/utils/withErrorPage'

interface ProposalPageProps {
  proposal?: Proposal
  voiceCount?: number
  initialVoices?: VoiceDto[] | null
  topics: Topic[]
  at: string
}

interface ProposalPagePropsAuthed {
  proposal: Proposal
}

const ProposalPage: AuthedFetchPage<
  ProposalPageProps,
  ProposalPagePropsAuthed
> = (props) => {
  const {
    proposal: { name, description, actions, popularVoices, _id },
    voiceCount,
    topics,
    at,
  } = props
  const { ouid } = useAuthContext()
  const voiceFilter = useMemo(() => {
    const ids = {}
    popularVoices.forEach((voice) => (ids[voice._id] = true))
    return (voice: VoiceDto) => !ids[voice._id]
  }, [popularVoices])

  const sortedTopics = useMemo(
    () =>
      topics.sort((topicA, topicB) => topicB.count - topicA.count).slice(0, 5),
    [topics]
  )

  const loadAfter: LoadAfter<VoiceDto> = useCallback(
    (count, voice) => {
      return getVoices(_id, { limit: count, afterVoiceId: voice?._id })
    },
    [_id]
  )

  return (
    <>
      <PageContainer floating={<CreateVoiceButton />}>
        <PageMeta
          title={name}
          titleHref={`/${_id}`}
          meta={{
            name,
            description: description
              .split(/<[^>]*>/g)
              .join('')
              .trim(),
            image: `${API_HOST}/wordcloud/${_id}/${at}`,
          }}
        />
        <Text
          as="h1"
          paddingX={8}
          marginTop="50px"
          fontSize="36px"
          fontWeight="bold"
          fontFamily="ChulaCharasNew"
          textAlign="center"
          color="chula.PrimaryPink"
        >
          {name}
        </Text>
        <Text
          marginY="10px"
          fontSize="24px"
          fontFamily="CHULALONGKORN"
          textAlign="center"
          color="chula.TransparentPink"
        >
          ร่วมเปลี่ยนแปลงไปกับ..
        </Text>
        <PinkCard
          color="chula.Gray"
          fontWeight="bold"
          textAlign="center"
          marginY="15px"
        >
          <Text fontSize="50px" fontFamily="ChulaCharasNew">
            <CountUp
              start={~~(voiceCount * 0.8)}
              end={voiceCount}
              separator=","
            />
          </Text>
          <Text fontSize="36px" fontFamily="ChulaCharasNew">
            เสียง
          </Text>
        </PinkCard>
        <Stack spacing={4} alignItems="center" marginY="50px">
          <Box>
            <Link href={`/${_id}/createVoice`}>
              <CustomButton
                width={145}
                height={45}
                variantColor="PrimaryPink"
                fontSize="22px"
                fontWeight="bold"
                rightIcon={AiFillSound}
              >
                ส่งเสียง
              </CustomButton>
            </Link>
          </Box>
          <Share
            title={name}
            url={`${APP_HOST}/${_id}`}
            shareButton={
              <CustomButton
                width={145}
                height={45}
                variant="outline"
                variantColor="Gold"
                color="chula.Copper"
                borderColor="chula.Copper"
                fontSize="22px"
                fontWeight="bold"
                rightIcon={AiOutlineShareAlt}
              >
                ส่งต่อ
              </CustomButton>
            }
          />
        </Stack>

        <SectionHead title="จุดประสงค์การรับฟังเสียง" />
        <ReadMore collapsedHeight={150} collapseThreshold={200}>
          <Box
            fontFamily="Sarabun"
            color="chula.Gray"
            style={{ textIndent: '2.5rem' }}
            dangerouslySetInnerHTML={{ __html: description }}
          />
        </ReadMore>

        {actions.length > 0 && (
          <>
            <SectionHead title="การตอบสนองต่อเสียงของนิสิต" />
            <ReadMore collapsedHeight={200} collapseThreshold={400}>
              <ActionsCard
                title={
                  <Text color="white" fontFamily="Sarabun" fontSize="14px">
                    สถานะล่าสุด: {actions[actions.length - 1].detail}
                  </Text>
                }
                actions={actions}
              />
            </ReadMore>
          </>
        )}

        {sortedTopics.length > 0 && (
          <>
            <SectionHead title="หัวข้อที่ถูกพูดถึงบ่อย" mb={2} />
            {sortedTopics.map((topic) => (
              <TopicItem key={topic.name} topic={topic} />
            ))}
            <TopicSelectModal topics={props.topics} isProposalPage />
          </>
        )}
        <SectionHead title="เสียงจากนิสิต" />
        {popularVoices.map((voice) => (
          <VoiceCard key={voice._id} voice={voice} />
        ))}
        <VoiceList
          queryKey={{ ouid, proposalId: _id, type: 'voices' }}
          loadAfter={loadAfter}
          initialVoices={null}
          voiceFilter={voiceFilter}
        />
      </PageContainer>
    </>
  )
}

ProposalPage.getInitialProps = async (query) => {
  const proposalId = query.proposalId as string
  const voiceCount = getVoiceCount(proposalId)
  const topics = getTopicsFromProposal(proposalId)
  const at = (query.at as string) || `${~~(+new Date() / 1000)}`

  return await promiseAllProperties({
    voiceCount,
    topics,
    at,
  })
}

ProposalPage.fetchAuthedData = async (query: ParsedUrlQuery) => {
  return { proposal: await getProposal(query.proposalId as string) }
}

ProposalPage.ContentWrapper = RouterProposalProvider

export function proposalPagePath(proposalId: string) {
  return `/${proposalId}`
}

export default withErrorPage(withAuthedFetch(ProposalPage))
