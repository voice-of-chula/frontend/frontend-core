import React, { useEffect } from 'react'
import { useAuthContext } from '@/providers/AuthProvider'
import { useRouter } from 'next/router'
import { PageSpinner } from '@/components/layout/PageSpinner'

export default function Logout() {
  const { isPending, logout } = useAuthContext()
  const router = useRouter()
  const { r } = router.query

  useEffect(() => {
    if (isPending) return
    logout().then(() => router.replace(`${r}`))
  }, [isPending, logout, router])

  return <PageSpinner />
}
