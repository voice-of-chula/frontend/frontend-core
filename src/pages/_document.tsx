import Document, { Head, Html, Main, NextScript } from 'next/document'
import { GA } from '@/utils/config'
import { PageMeta } from '@/utils/pageMeta'

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    PageMeta.rewind()
    return { ...initialProps }
  }

  render() {
    return (
      <Html lang="th">
        <Head>
          <link rel="preconnect" href="https://www.google-analytics.com" />
        </Head>
        <body>
          <Main />
          <NextScript />
          <script
            async
            src={`https://www.googletagmanager.com/gtag/js?id=${GA}`}
          />
          <script
            dangerouslySetInnerHTML={{
              __html: `
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());
              gtag('config', '${GA}');
              `,
            }}
          />
          <script src="https://static.airtable.com/js/embed/embed_snippet_v1.js"></script>
        </body>
      </Html>
    )
  }
}

export default MyDocument
