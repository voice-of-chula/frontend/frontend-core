import React from 'react'
import { CustomSpinner } from '@/components/common/CustomSpinner'
import { PageContainer } from '@/components/layout/PageContainer'

const RequestPage = () => {
  return (
    <PageContainer position="relative">
      <CustomSpinner position="absolute" top={20} zIndex={-1} />
      <iframe
        className="airtable-embed airtable-dynamic-height"
        src="https://airtable.com/embed/shrvGKrso4M5Q4mzl?backgroundColor=pink"
        frameBorder="0"
        width="100%"
        height="945"
        style={{ background: 'transparent' }}
      />
    </PageContainer>
  )
}

export default RequestPage
