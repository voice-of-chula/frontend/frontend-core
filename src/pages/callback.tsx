import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { exists } from '@/utils/types'
import { exchangeTicket } from '@/utils/api/auth'
import { useAuthContext } from '@/providers/AuthProvider'
import { PageSpinner } from '@/components/layout/PageSpinner'

export default function CallbackPage() {
  const router = useRouter()
  const { login, isAuthenticated } = useAuthContext()
  let { ticket, r } = router.query
  const [inProgress, setProgress] = useState<boolean>(false)

  useEffect(() => {
    if (!ticket && r) {
      ticket = r.slice(r.indexOf('?') + 1 + 7)
    }
  }, [r])

  useEffect(() => {
    if (exists(ticket) && !isAuthenticated && !inProgress) {
      setProgress(true)

      exchangeTicket(ticket as string)
        .then(({ accessToken }) => {
          login(accessToken)
          router.replace(`${r}`)
        })
        .finally(() => setProgress(false))
        .catch((e) => {
          alert(e)
        })
    } else if (isAuthenticated) {
      router.replace(`${r}`)
    }
  }, [router, ticket, inProgress, isAuthenticated, inProgress, login])

  return <PageSpinner />
}
