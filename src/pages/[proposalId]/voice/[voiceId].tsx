import { CustomModal } from '@/components/common/CustomModal'
import { CustomModalOverlay } from '@/components/common/CustomModalOverlay'
import { PageContainer } from '@/components/layout/PageContainer'
import { ReplyBox, ReplyList } from '@/components/voice/ReplyList'
import { useRequireLogin } from '@/components/common/RequireLoginModal'
import { SectionHead } from '@/components/common/SectionHead'
import { Share } from '@/components/common/Share'
import { StickyBox } from '@/components/common/StickyBox'
import { TopicBoxGroup } from '@/components/topic/TopicBox'
import { VoiceDetail } from '@/components/voice/VoiceDetail'
import {
  RouterProposalProvider,
  useProposalContext,
} from '@/providers/ProposalProvider'
import { useFormatTime, useOwnerInfo } from '@/utils'
import { getProposal, Proposal } from '@/utils/api/proposal'
import { getVoice, VoiceDto } from '@/utils/api/voice'
import { API_HOST, APP_HOST } from '@/utils/config'
import { PageMeta } from '@/utils/pageMeta'
import promiseAllProperties from '@/utils/promiseAllProperties'
import { useVoice } from '@/utils/voiceModel'
import { AuthedFetchPage, withAuthedFetch } from '@/utils/withAuthedFetch'
import { withErrorPage } from '@/utils/withErrorPage'
import {
  Box,
  BoxProps,
  Button,
  ButtonProps,
  Flex,
  IconButton,
  Link,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  Stack,
  Text,
  useDisclosure,
} from '@chakra-ui/core'
import NextLink from 'next/link'
import { useRouter } from 'next/router'
import React from 'react'
import {
  AiFillDislike,
  AiFillLike,
  AiOutlineDislike,
  AiOutlineLike,
} from 'react-icons/ai'
import { FiFlag, FiShare2 } from 'react-icons/fi'
import { IconType } from 'react-icons/lib'

interface VoicePageProps {
  voice: VoiceDto
  proposal: Proposal
  at: string
}

interface ActionButtonProps {
  icon: IconType
}

const ActionButton: React.FC<ActionButtonProps & ButtonProps> = ({
  icon: IconComponent,
  children,
  ...rest
}) => (
  <Button color="chula.Gray" variant="link" py={2} {...rest}>
    <Box color="chula.PrimaryPink">
      <IconComponent size="20px" />
    </Box>
    <Text
      fontFamily="ChulaCharasNew"
      fontWeight="bold"
      fontSize="20px"
      color="chula.Gray"
      ml={2}
    >
      {children}
    </Text>
  </Button>
)

interface ActionCountProps {
  count: number
  title: string
}

const ActionCount: React.FC<ActionCountProps & BoxProps> = ({
  count,
  title,
  ...rest
}) => (
  <Box {...rest}>
    <Text
      display="inline-block"
      fontFamily="CHULALONGKORN"
      fontWeight="bold"
      fontSize="20px"
      color="chula.Gray"
    >
      {title}
    </Text>
    <Text
      display="inline-block"
      fontFamily="CHULALONGKORN"
      fontWeight="bold"
      fontSize="24px"
      color="chula.PrimaryPink"
      ml={2}
    >
      {count}
    </Text>
  </Box>
)

const VoicePage: AuthedFetchPage<{}, VoicePageProps> = ({
  voice: voiceDto,
  proposal,
  at,
}: VoicePageProps) => {
  const {
    voice,
    toggleUpvote,
    toggleDownvote,
    onReport,
    voiceURL,
    replies,
    replyCount,
    submitReply,
    loadMoreReplies,
    modals,
    loginURL,
  } = useVoice(voiceDto)
  const {
    _id: voiceId,
    owner,
    detail,
    topics,
    upvotes,
    downvotes,
    selfUpvoted,
    selfDownvoted,
    createdTime,
  } = voice
  const ownerInfo = useOwnerInfo(owner)
  const formattedTime = useFormatTime(createdTime)

  const { isOpen, onClose, onToggle: onToggleReport } = useDisclosure()
  const { func: onToggleReportWithLogin, modal } = useRequireLogin(
    'รายงานเสียงนี้',
    onToggleReport
  )

  const onConfirmReport = () => {
    onReport()
    onClose()
  }

  const {
    push,
    query: { sharingPopup },
  } = useRouter()

  const { proposalId } = useProposalContext()

  const {
    isOpen: isSharingModalOpen,
    onOpen: onSharingModalOpen,
    onClose: onSharingModalClose,
  } = useDisclosure(false)

  if (sharingPopup == 'true') {
    setTimeout(onSharingModalOpen, 1000)
    push(voicePagePath(proposalId, voiceId), undefined, {
      shallow: true,
    })
  }

  return (
    <PageContainer>
      <CustomModal isOpen={isSharingModalOpen} onClose={onSharingModalClose}>
        <CustomModalOverlay />
        <ModalContent w="90%">
          <ModalHeader fontFamily="ChulaCharasNew">
            ส่งเสียงเสร็จแล้ว ส่งต่อด้วยเลยสิ!
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody fontFamily="Sarabun">
            ส่งต่อประเด็นนี้ให้เป็นที่รับรู้{' '}
            และเชิญชวนเพื่อนของเราให้เข้ามาส่งเสียงด้วยกัน!
          </ModalBody>

          <ModalFooter>
            <Button
              fontFamily="ChulaCharasNew"
              variant="ghost"
              mr={3}
              onClick={onSharingModalClose}
            >
              ไว้ทีหลัง
            </Button>
            <Share
              title={proposal.name}
              url={`${APP_HOST}/${proposal._id}`}
              shareButton={
                <Button fontFamily="ChulaCharasNew" variantColor="PrimaryPink">
                  ส่งต่อเลย!
                </Button>
              }
            />
          </ModalFooter>
        </ModalContent>
      </CustomModal>
      <PageMeta
        title={proposal.name}
        titleHref={`/${proposal._id}`}
        meta={{
          name: `เสียงจากนิสิต ${ownerInfo}`,
          description: detail,
          image: `${API_HOST}/wordcloud/${proposal._id}/${at}`,
        }}
      />
      <CustomModal isOpen={isOpen} onClose={onClose}>
        <CustomModalOverlay />
        <ModalContent>
          <ModalHeader fontFamily="ChulaCharasNew">
            คุณต้องการที่จะรายงานเสียงนี้?
          </ModalHeader>
          <ModalCloseButton />
          <ModalFooter>
            <Button
              fontFamily="ChulaCharasNew"
              variant="ghost"
              mr={3}
              onClick={onClose}
            >
              ยกเลิก
            </Button>
            <Button
              fontFamily="ChulaCharasNew"
              variantColor="PrimaryPink"
              onClick={onConfirmReport}
            >
              ยืนยัน
            </Button>
          </ModalFooter>
        </ModalContent>
      </CustomModal>
      <Flex flexDirection="column" flex={1}>
        <Box flex={1}>
          <SectionHead title="เสียงของนิสิต" subtitle={ownerInfo} />
          <Flex marginTop="-8px" alignItems="center">
            <TopicBoxGroup mt={2} topics={topics} />
            <Box flex={1} />
            <IconButton
              height="2rem"
              aria-label=""
              icon={FiFlag}
              variant="ghost"
              variantColor="PrimaryPink"
              color="Gray.200"
              onClick={onToggleReportWithLogin}
            />
          </Flex>
          <VoiceDetail voice={voice} />
          <NextLink href={voiceURL} passHref>
            <Link
              fontFamily="Sarabun"
              fontSize="16px"
              color="chula.Gray"
              mt={2}
            >
              {formattedTime}
            </Link>
          </NextLink>
          <Stack isInline={true} spacing={8}>
            <ActionButton
              icon={selfUpvoted ? AiFillLike : AiOutlineLike}
              onClick={toggleUpvote}
            >
              เห็นด้วย
            </ActionButton>
            <ActionButton
              icon={selfDownvoted ? AiFillDislike : AiOutlineDislike}
              onClick={toggleDownvote}
            >
              ไม่เห็นด้วย
            </ActionButton>
            <Share
              title="เสียงของนิสิต"
              url={voiceURL}
              shareButton={<ActionButton icon={FiShare2}>แชร์</ActionButton>}
            />
          </Stack>
          <Box w="100%" h="1px" bg="chula.Gold" />
          <Stack isInline={true} spacing={4}>
            <ActionCount count={upvotes} title="เห็นด้วย" />
            <ActionCount count={downvotes} title="ไม่เห็นด้วย" />
            <ActionCount count={replyCount} title="ความคิดเห็น" />
          </Stack>
          <Box w="100%" h="1px" bg="chula.Gold" />
          <ReplyList replies={replies} loadMoreReplies={loadMoreReplies} />
        </Box>
        <StickyBox pt={2} bottom="16px">
          <ReplyBox loginURL={loginURL} onSubmit={submitReply} />
        </StickyBox>
      </Flex>
      {modals} {modal}
    </PageContainer>
  )
}

VoicePage.fetchAuthedData = async (query) => {
  const proposalId = query.proposalId as string
  const voiceId = query.voiceId as string
  const at = (query.at as string) || `${~~(+new Date() / 1000)}`

  return await promiseAllProperties({
    proposal: getProposal(proposalId),
    voice: getVoice(proposalId, voiceId),
    at,
  })
}

VoicePage.ContentWrapper = RouterProposalProvider

export function voicePagePath(proposalId: string, voiceId: string) {
  return `/${proposalId}/voice/${voiceId}`
}

export default withErrorPage(withAuthedFetch(VoicePage))
