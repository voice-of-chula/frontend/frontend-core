import { CreateVoiceModal } from '@/components/voice/CreateVoiceModal'
import { CustomButton } from '@/components/common/CustomButton'
import { PageContainer } from '@/components/layout/PageContainer'
import { withAuth } from '@/providers/AuthProvider'
import { RouterProposalProvider } from '@/providers/ProposalProvider'
import {
  getProposal,
  getTopicsFromProposal,
  Proposal,
  Topic,
} from '@/utils/api/proposal'
import { createVoice as submitVoice } from '@/utils/api/voice'
import { PageMeta } from '@/utils/pageMeta'
import promiseAllProperties from '@/utils/promiseAllProperties'
import { withErrorPage } from '@/utils/withErrorPage'
import { QueryCachePage, withQueryCache } from '@/utils/withQueryCache'
import { Box, Flex, Text, Textarea } from '@chakra-ui/core'
import { useRouter } from 'next/router'
import { useCallback, useState } from 'react'
import { voicePagePath } from './voice/[voiceId]'

interface CreateVoicePageProps {
  proposal: Proposal
  allTopics: Topic[]
}

const CreateVoice: React.FC<CreateVoicePageProps> = (props) => {
  const { proposal, allTopics } = props
  const [detail, setDetail] = useState('')
  const [topics, setTopics] = useState<string[]>([])
  const [loading, setLoading] = useState(false)
  const router = useRouter()
  const proposalId = router.query.proposalId as string

  const handleChangeUserVoice = useCallback(({ target }) => {
    setDetail(target.value)
  }, [])

  const handleSubmitVoice = useCallback(
    async (e) => {
      e.preventDefault()
      if (loading) {
        return
      }
      setLoading(true)
      try {
        const res = await submitVoice(proposalId, {
          detail,
          topics,
        })
        const pathname = voicePagePath(proposalId, res._id)
        router.push(
          {
            pathname,
            query: {
              sharingPopup: true,
            },
          },
          pathname
        )
      } catch (err) {
        // TODO: better error reporting
        alert('Failed to create voice')
      } finally {
        setLoading(false)
      }
    },
    [proposalId, detail, topics, loading]
  )

  return (
    <RouterProposalProvider>
      <PageContainer>
        <PageMeta title={proposal.name} titleHref={`/${proposal._id}`} />
        <Flex direction="column" py="16px">
          <Text
            mt="8px"
            mb="12px"
            fontFamily="ChulaCharasNew"
            fontWeight="bold"
            fontSize="24px"
            color="chula.PrimaryPink"
          >
            หัวข้อที่เกี่ยวข้องกับเสียงนี้ ({topics.length}/3)
          </Text>

          <Box mb="24px">
            <CreateVoiceModal
              topics={topics}
              allTopics={allTopics}
              setTopics={setTopics}
            />
          </Box>

          <Text
            mb="16px"
            fontFamily="ChulaCharasNew"
            fontWeight="bold"
            fontSize="20px"
            color="chula.PrimaryPink"
          >
            เสียงของคุณ
          </Text>

          <Textarea
            h="312px"
            resize="none"
            borderColor="chula.SecondaryPink"
            focusBorderColor="chula.PrimaryPink"
            rounded="15px"
            fontFamily="Sarabun"
            onChange={handleChangeUserVoice}
          />

          <Flex justify="center">
            <CustomButton
              mt="24px"
              w="133px"
              h="45px"
              variantColor="PrimaryPink"
              onClick={handleSubmitVoice}
              isDisabled={loading}
            >
              ส่งเสียง
            </CustomButton>
          </Flex>
        </Flex>
      </PageContainer>
    </RouterProposalProvider>
  )
}

const CreateVoiceWithAuth: QueryCachePage<CreateVoicePageProps> = withAuth(
  CreateVoice
)

CreateVoiceWithAuth.getInitialProps = async (query) => {
  const id = query.proposalId as string
  const proposal = getProposal(id)
  const allTopics = getTopicsFromProposal(id)
  return await promiseAllProperties({ proposal, allTopics })
}

export default withErrorPage(withQueryCache(CreateVoiceWithAuth))
