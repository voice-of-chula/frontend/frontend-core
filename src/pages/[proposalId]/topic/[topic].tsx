import { Box } from '@chakra-ui/core'
import React, { useCallback } from 'react'
import { CreateVoiceButton } from '@/components/voice/CreateVoiceButton'
import { PageContainer } from '@/components/layout/PageContainer'
import { SectionHead } from '@/components/common/SectionHead'
import TopicSelectModal from '@/components/topic/TopicSelectModal'
import { VoiceList } from '@/components/voice/VoiceList'
import { useAuthContext } from '@/providers/AuthProvider'
import {
  ProposalProvider,
  RouterProposalProvider,
} from '@/providers/ProposalProvider'
import { LoadAfter } from '@/utils'
import {
  getProposal,
  getTopicsFromProposal,
  Proposal,
  Topic,
} from '@/utils/api/proposal'
import {
  DEFAULT_VOICE_COUNT,
  getVoicesFromTopic,
  VoiceDto,
} from '@/utils/api/voice'
import { API_HOST } from '@/utils/config'
import { PageMeta } from '@/utils/pageMeta'
import promiseAllProperties from '@/utils/promiseAllProperties'
import { AuthedFetchPage, withAuthedFetch } from '@/utils/withAuthedFetch'
import { withErrorPage } from '@/utils/withErrorPage'

interface TopicPageProps {
  topic: string
  topics: Topic[]
}

interface TopicPagePropsAuthed {
  proposal: Proposal
  initialVoices: VoiceDto[]
}

const TopicPage: AuthedFetchPage<TopicPageProps, TopicPagePropsAuthed> = (
  props
) => {
  const { proposal, initialVoices, topic, topics } = props
  const { ouid } = useAuthContext()

  const loadAfter: LoadAfter<VoiceDto> = useCallback(
    (count, voice) => {
      return getVoicesFromTopic(proposal._id, topic as string, {
        limit: count,
        afterVoiceId: voice?._id,
      })
    },
    [proposal._id]
  )

  return (
    <>
      <PageContainer floating={<CreateVoiceButton />}>
        <PageMeta
          title={proposal.name}
          titleHref={`/${proposal._id}`}
          meta={{
            name: `เสียงจากนิสิตในประเด็น ${topic} หัวข้อ${proposal.name}`,
            description: proposal.description
              .split(/<[^>]*>/g)
              .join('')
              .trim(),
            image: `${API_HOST}/wordcloud/${proposal._id}/${~~(
              +new Date() / 1000
            )}`,
          }}
          searchButton={
            <ProposalProvider proposalId={proposal._id}>
              <TopicSelectModal topics={topics} />
            </ProposalProvider>
          }
        />
        <SectionHead title={`เสียงจากนิสิตในหัวข้อ: ${topic}`} />
        <VoiceList
          queryKey={{
            ouid,
            proposalId: proposal._id,
            type: 'topic-voices',
            topic,
          }}
          loadAfter={loadAfter}
          initialVoices={initialVoices}
        />
        <Box flex={1} />
      </PageContainer>
    </>
  )
}

TopicPage.getInitialProps = async (query) => {
  const proposalId = query.proposalId as string
  const topic = query.topic as string
  return { topic, topics: await getTopicsFromProposal(proposalId) }
}

TopicPage.fetchAuthedData = async (query) => {
  const proposalId = query.proposalId as string
  const topic = query.topic as string
  return await promiseAllProperties({
    proposal: getProposal(query.proposalId as string),
    initialVoices: getVoicesFromTopic(proposalId, topic, {
      limit: DEFAULT_VOICE_COUNT,
    }),
  })
}

TopicPage.ContentWrapper = RouterProposalProvider

export function topicPagePath(proposalId: string, topic: string) {
  return `/${proposalId}/topic/${topic}`
}

export default withErrorPage(withAuthedFetch(TopicPage))
