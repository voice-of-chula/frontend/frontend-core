import { useCallback } from 'react'
import { PageContainer } from '@/components/layout/PageContainer'
import { SectionHead } from '@/components/common/SectionHead'
import { VoiceDto, getMyVoices } from '@/utils/api/voice'
import { CreateVoiceButton } from '@/components/voice/CreateVoiceButton'
import { useRouter } from 'next/router'
import { RouterProposalProvider } from '@/providers/ProposalProvider'
import { VoiceList } from '@/components/voice/VoiceList'
import { LoadAfter } from '@/utils'
import { Box } from '@chakra-ui/core'
import { withAuth, useAuthContext } from '@/providers/AuthProvider'

function myVoice() {
  const router = useRouter()
  const proposalId = router.query.proposalId as string
  const { ouid } = useAuthContext()

  const loadAfter: LoadAfter<VoiceDto> = useCallback(
    (count, voice) => {
      return getMyVoices(proposalId, { limit: count, afterVoiceId: voice?._id })
    },
    [proposalId]
  )

  return (
    <>
      <PageContainer floating={<CreateVoiceButton />}>
        <SectionHead title="เสียงของฉัน" />
        <VoiceList
          queryKey={{ ouid, proposalId, type: 'myVoices' }}
          loadAfter={loadAfter}
          initialVoices={null}
        />
        <Box flex={1} />
      </PageContainer>
    </>
  )
}

export default withAuth(myVoice, RouterProposalProvider)
