import { Text } from '@chakra-ui/core'
import CountUp from 'react-countup'
import { PageContainer } from '@/components/layout/PageContainer'
import { PinkCard } from '@/components/common/PinkCard'
import { ProposalCard } from '@/components/proposal/ProposalCard'
import { SectionHead } from '@/components/common/SectionHead'
import { getProposalsWithCount, Proposal } from '@/utils/api/proposal'
import { withQueryCache, QueryCachePage } from '@/utils/withQueryCache'

interface HomeProps {
  totalVoices: number
  proposals: Proposal[]
}

const Home: QueryCachePage<HomeProps> = (props) => {
  return (
    <PageContainer>
      <PinkCard>
        <Text
          fontFamily="CHULALONGKORN"
          fontSize={28}
          color="chula.PrimaryPink"
        >
          ร่วม <b>'ส่งเสียง'</b>
          <br />
          เพื่อ <b>'เปลี่ยนแปลง'</b>
        </Text>
        <Text fontFamily="CHULALONGKORN" fontSize={28} color="chula.Gray">
          ไปกับ{' '}
          <Text fontFamily="ChulaCharasNew" fontSize={52} as="span">
            <CountUp
              start={~~(props.totalVoices * 0.8)}
              end={props.totalVoices}
              separator=","
            />
          </Text>{' '}
          เสียง
        </Text>
      </PinkCard>
      <SectionHead title="หัวข้อรับฟังเสียงจากนิสิต" />
      {props.proposals.map((proposal) => (
        <ProposalCard key={proposal._id} proposal={proposal} />
      ))}
    </PageContainer>
  )
}

Home.getInitialProps = async () => {
  const result = await getProposalsWithCount()

  return {
    totalVoices: result.count,
    proposals: result.proposals,
  }
}

export default withQueryCache(Home)
