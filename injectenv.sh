#! /bin/sh

REF=$(sed 's/.*\/\([^\/]*\)$/\1/' < .git/HEAD)

echo Current ref $REF

case $REF in
    deploy)
        export NEXT_PUBLIC_APP_HOST=https://voice-of-chula.online
        export NEXT_PUBLIC_API_HOST=https://voice-of-chula.online/api
        export NEXT_PUBLIC_SSO_URL=https://account.it.chula.ac.th
        ;;
    dev)
        export NEXT_PUBLIC_APP_HOST=https://voc.isd-sgcu.dev
        export NEXT_PUBLIC_API_HOST=https://voc.isd-sgcu.dev/api
        export NEXT_PUBLIC_SSO_URL=https://chulassomock-jjz7jmptma-uc.a.run.app
        ;;
    *)
        echo No match
        exit 1
        ;;
esac
